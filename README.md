# ACH Edit

A graphical editor for ACH files.

## Building

Currently building on Windows is only supported.

### Requirements

- .NET Core SDK >= 3.1 ([Latest SDK](https://dotnet.microsoft.com/download))

Visual Studio and/or VSCodes is not required to build.

MacOS and Linux are _not_ supported.

#### Recommendations

It is recommended that you use the latest available build of Windows 10 to build the application. Other versions of Windows (7, 8, 8.1, Server Editions) have _not_ been verified for compatibility.

### Create Release Build

Run the build script (build.ps1) under the "scripts" directory.

From the root of the repository run:

```powershell
.\scripts\build.ps1
```

A directory will be created in the repository called build. If the build was successful this will contain the final, unpacked application.

### Packaging

Currently a .zip package is created by the CI/CD pipeline.

We are investigating using MSIX as an installation/deployment option. (See Issue #12)