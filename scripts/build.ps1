param(
    [string] $Configuration = 'Release',
    [string] $Runtime       = 'win-x64'
)

$RootDir  = [System.IO.Path]::GetFullPath("$PSScriptRoot\..")
$BuildDir = [System.IO.Path]::Combine($RootDir, "build")
$ProjFile = [System.IO.Path]::Combine($RootDir, "src", "AchEdit", "AchEdit.csproj")

if (Test-Path $BuildDir) {
    Remove-Item -Force -Recurse "$RootDir\build\*"
}

dotnet publish -c $Configuration -r $Runtime -o $BuildDir $ProjFile
