using System;
using System.IO;
using System.Linq;
using System.Windows;
using AchEdit.Configuration;
using Serilog;
using Serilog.Events;
using Serilog.Exceptions;
using Serilog.Formatting.Json;

namespace AchEdit
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static IConfigRepository Config { get; private set; }

        public static FilePathsConfig FilePaths { get; } = new FilePathsConfig();

        public static Version Version { get; }

        static App()
        {
            Version = typeof(App).Assembly.GetName().Version;
        }

        public IntPtr GetMainWindowHandle()
        {
            return GetMainWindow()?.Handle ?? IntPtr.Zero;
        }

        public MainWindow GetMainWindow()
        {
            return Windows.OfType<MainWindow>().FirstOrDefault();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            EnsureDirectories();
            SetupLogging();
            base.OnStartup(e);

            Config = ConfigRepository.LoadConfiguration(FilePaths.ConfigFile);

            Log.Information("Application started.");
        }

        /// <summary>
        /// Ensures that directories that we need in order to run actually exist.
        /// </summary>
        private void EnsureDirectories()
        {
            if (!Directory.Exists(FilePaths.AppDataDir))
                Directory.CreateDirectory(FilePaths.AppDataDir);

            if (!Directory.Exists(FilePaths.AppDataLocalDir))
                Directory.CreateDirectory(FilePaths.AppDataLocalDir);
        }

        private void SetupLogging()
        {
            const string template = "[{Timestamp:HH:mm:ss}][{Level}]({SourceContext}) {Message:lj}{NewLine}{Exception}";

            var logFilePath = Path.Combine(FilePaths.LogsDir, "achedit-{Date}.log");
            var jsonFilePath = Path.Combine(FilePaths.LogsDir, "achedit-{Date}.json");

            Log.Logger = new LoggerConfiguration()
                .Enrich.WithExceptionDetails()
                .MinimumLevel.Is(LogEventLevel.Verbose)
                .WriteTo.RollingFile(logFilePath,
                    outputTemplate: template,
                    shared: true,
                    retainedFileCountLimit: 7)
                .WriteTo.RollingFile(new JsonFormatter(), jsonFilePath,
                    shared: true,
                    retainedFileCountLimit: 7)
                .CreateLogger()
                .ForContext("SourceContext", "AchEdit");
        }
    }
}
