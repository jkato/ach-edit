using System.Windows.Input;

namespace AchEdit.Commands
{
    public static class AchEditCommands
    {
        public static RoutedCommand DeleteRecord { get; } = new RoutedCommand
        {
            InputGestures =
            {
                new KeyGesture(Key.Delete),
            }
        };

        public static RoutedCommand EditRecord { get; } = new RoutedCommand
        {
            InputGestures =
            {
                new KeyGesture(Key.F2),
                new MouseGesture(MouseAction.LeftDoubleClick),
            }
        };

        public static RoutedCommand EditRawRecord { get; } = new RoutedCommand
        {
            InputGestures =
            {
                new KeyGesture(Key.F2, ModifierKeys.Control),
            }
        };

        public static RoutedCommand GoToRecord { get; } = new RoutedCommand
        {
            InputGestures =
            {
                new KeyGesture(Key.G, ModifierKeys.Control),
            }
        };

        public static RoutedCommand NavigateToLink { get; } = new RoutedCommand();

        public static RoutedCommand SubmitRecordIndex { get; } = new RoutedCommand();
    }
}
