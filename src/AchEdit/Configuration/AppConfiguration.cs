using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace AchEdit.Configuration
{
    public class AppConfiguration : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public FileConfig FileConfig { get; set; } = new FileConfig();

        [JsonPropertyName("recordColors")]
        public RecordColorsConfig RecordColors { get; set; } = new RecordColorsConfig();

        internal void WriteJson(Utf8JsonWriter writer)
        {
            writer.WriteStartObject();

            writer.WriteStartObject("filesConfig");
            FileConfig.WriteJson(writer);
            writer.WriteEndObject();

            writer.WriteStartObject("recordColors");
            RecordColors.WriteJson(writer);
            writer.WriteEndObject();

            writer.WriteEndObject();
        }

        protected virtual void HandleOnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
