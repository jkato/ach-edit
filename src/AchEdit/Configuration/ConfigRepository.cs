using System;
using System.IO;
using System.Text.Json;
using System.Windows.Media;
using Microsoft.Extensions.Configuration;
using Nacho;

namespace AchEdit.Configuration
{
    public interface IConfigRepository
    {
        FileConfig GetFileConfig();

        RecordColorsConfig GetRecordColors();

        void Save(string filePath);
    }

    public class ConfigRepository : IConfigRepository
    {
        public static void CreateDefaultConfiguration(string filePath)
        {
            SaveConfiguration(new AppConfiguration(), filePath);
        }

        public static ConfigRepository LoadConfiguration(string filePath)
        {
            if (!File.Exists(filePath))
                CreateDefaultConfiguration(filePath);

            var config = new ConfigurationBuilder()
                .AddJsonFile(filePath, false, true)
                .Build();

            return new ConfigRepository(config);
        }

        public static void SaveConfiguration(AppConfiguration config, string filePath)
        {
            using var fs = File.Open(filePath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Write);
            using var writer = new Utf8JsonWriter(fs, new JsonWriterOptions
            {
                Indented = true,
            });

            config.WriteJson(writer);
            writer.Flush();
        }

        private IConfiguration Config { get; }

        private AppConfiguration AppConfig { get; } = new AppConfiguration();

        private Lazy<FileConfig> LazyFileConfig { get; }

        private Lazy<RecordColorsConfig> LazyRecordColorsConfig { get; }

        public ConfigRepository(IConfiguration config)
        {
            Config = config;
            LazyFileConfig = new Lazy<FileConfig>(CreateFileConfig);
            LazyRecordColorsConfig = new Lazy<RecordColorsConfig>(CreateRecordColorsConfig);
        }

        public FileConfig GetFileConfig()
        {
            return LazyFileConfig.Value;
        }

        public RecordColorsConfig GetRecordColors()
        {
            return LazyRecordColorsConfig.Value;
        }

        public void Save(string filePath)
        {
            SaveConfiguration(AppConfig, filePath);
        }

        private FileConfig CreateFileConfig()
        {
            var config = new FileConfig();
            try
            {
                var lineEndingStr = Config.GetSection("filesConfig")["defaultLineEnding"];
                config.DefaultLineEnding = Enum.Parse<LineEnding>(lineEndingStr);
            }
            catch (Exception)
            {
                return new FileConfig();
            }

            return AppConfig.FileConfig = config;
        }

        private RecordColorsConfig CreateRecordColorsConfig()
        {
            var colors = new RecordColorsConfig();

            try
            {
                var code = Config.GetSection("recordColors")["file"];
                colors.FileRecord = (Color) (ColorConverter.ConvertFromString(code) ?? Colors.Red);

                code = Config.GetSection("recordColors")["batch"];
                colors.BatchRecord = (Color) (ColorConverter.ConvertFromString(code) ?? Colors.Red);

                code = Config.GetSection("recordColors")["detailCredit"];
                colors.DetailCredit = (Color) (ColorConverter.ConvertFromString(code) ?? Colors.Red);

                code = Config.GetSection("recordColors")["detailDebit"];
                colors.DetailDebit = (Color) (ColorConverter.ConvertFromString(code) ?? Colors.Red);

                code = Config.GetSection("recordColors")["addenda"];
                colors.Addenda = (Color)(ColorConverter.ConvertFromString(code) ?? Colors.Red);
            }
            catch (Exception)
            {
                return new RecordColorsConfig();
            }

            return AppConfig.RecordColors = colors;
        }
    }
}
