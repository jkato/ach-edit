using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text.Json;
using System.Text.Json.Serialization;
using Nacho;

namespace AchEdit.Configuration
{
    public class FileConfig : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The default line ending used when saving files to disk.
        /// </summary>
        [JsonPropertyName("defaultLineEnding")]
        public LineEnding DefaultLineEnding { get; set; } = LineEnding.CrLf;

        internal void WriteJson(Utf8JsonWriter writer)
        {
            writer.WritePropertyName("defaultLineEnding");
            writer.WriteStringValue(DefaultLineEnding.ToString());
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
