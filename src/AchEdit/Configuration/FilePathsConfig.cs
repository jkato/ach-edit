using System;
using System.IO;

namespace AchEdit.Configuration
{
    public class FilePathsConfig
    {
        public FilePathsConfig Instance => LazyInstance.Value;

        private static string AppDataRoot { get; }

        private static string AppDataLocalRoot { get; }

        private static Lazy<FilePathsConfig> LazyInstance { get; } =
            new Lazy<FilePathsConfig>(() => new FilePathsConfig());

        static FilePathsConfig()
        {
            AppDataRoot = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            AppDataLocalRoot = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
        }

        public string AppDataDir { get; }

        public string AppDataLocalDir { get; }

        public string LogsDir { get; }

        public string ConfigFile { get; }

        public FilePathsConfig()
        {
            AppDataDir = Path.Combine(AppDataRoot, "AchEdit");
            AppDataLocalDir = Path.Combine(AppDataLocalRoot, "AchEdit");
            LogsDir = Path.Combine(AppDataLocalDir, "logs");

            ConfigFile = Path.Combine(AppDataDir, "config.json");
        }
    }
}
