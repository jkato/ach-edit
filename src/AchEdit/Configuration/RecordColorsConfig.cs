using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Windows.Media;

namespace AchEdit.Configuration
{
    public class RecordColorsConfig : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public static Color DefaultFileRecord { get; } = Colors.LightBlue;

        public static Color DefaultBatchRecord { get; } = Colors.LightSkyBlue;

        public static Color DefaultDetailCredit { get; } = Colors.LightGreen;

        public static Color DefaultDetailDebit { get; } = Colors.LightCoral;

        public static Color DefaultAddenda { get; } = Colors.DarkGray;

        [JsonPropertyName("file")]
        public Color FileRecord { get; set; } = DefaultFileRecord;

        [JsonPropertyName("batch")]
        public Color BatchRecord { get; set; } = DefaultBatchRecord;

        [JsonPropertyName("detailCredit")]
        public Color DetailCredit { get; set; } = Colors.LightGreen;

        [JsonPropertyName("detailDebit")]
        public Color DetailDebit { get; set; } = Colors.LightCoral;

        [JsonPropertyName("addenda")]
        public Color Addenda { get; set; } = Colors.DarkGray;

        internal void WriteJson(Utf8JsonWriter writer)
        {
            writer.WritePropertyName("file");
            writer.WriteStringValue(FileRecord.ToString());

            writer.WritePropertyName("batch");
            writer.WriteStringValue(BatchRecord.ToString());

            writer.WritePropertyName("detailCredit");
            writer.WriteStringValue(DetailCredit.ToString());

            writer.WritePropertyName("detailDebit");
            writer.WriteStringValue(DetailDebit.ToString());

            writer.WritePropertyName("addenda");
            writer.WriteStringValue(Addenda.ToString());
        }

        protected virtual void HandleOnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
