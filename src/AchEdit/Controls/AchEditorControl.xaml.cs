using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using AchEdit.Data;
using AchEdit.Providers;
using AchEdit.ViewModels;
using AchEdit.Windows;
using Nacho;

namespace AchEdit.Controls
{
    /// <summary>
    /// Interaction logic for AchEditorControl.xaml
    /// </summary>
    public partial class AchEditorControl
    {
        public string FileName => ViewModel.AchFile?.Name ?? "New File";

        public bool ReadOnly => ViewModel.ReadOnly;

        public AchEditorViewModel ViewModel => (AchEditorViewModel) DataContext;

        private IDialogsProvider Dialogs { get; }

        public AchEditorControl()
        {
            InitializeComponent();

            Dialogs = new DialogsProvider();
        }

        public AchEditorControl(FileInfo file, IEnumerable<AchRecord> records)
            : this()
        {
            ViewModel.Document = new AchDocument(file, records);
        }

        public AchEditorControl(AchDocument doc)
            : this()
        {
            ViewModel.Document = doc;
        }

        public void HandleRecordCut(object sender, RoutedEventArgs args)
        {

        }

        public void HandleRecordCopy(object sender, RoutedEventArgs args)
        {
            var buffer = new StringBuilder();
            foreach (var item in RecordsListView.SelectedItems)
            {
                if (item is AchRecord rec)
                {
                    buffer.AppendLine(rec.RecordData.Span.ToString());
                }
            }

            Clipboard.SetText(buffer.ToString());
        }

        public void HandleRecordDelete(object sender, RoutedEventArgs args)
        {
            ViewModel.Records.RemoveAt(ViewModel.SelectedRecordIndex);
        }

        protected void HandleGoToRecord(object sender, RoutedEventArgs args)
        {
            var dialog = new GoToRecordWindow
            {
                Owner = ((App) Application.Current).GetMainWindow(),
            };

            var result = dialog.ShowDialog() ?? false;
            if (!result)
                return;

            ViewModel.SelectedRecordIndex = dialog.RecordIndex;
            RecordsListView.ScrollIntoView(ViewModel.SelectedRecord);
        }

        protected void HandleRecordEdit(object sender, RoutedEventArgs args)
        {
            args.Handled = true;
            var editor = new RecordEditorWindow(ViewModel.SelectedRecord, ViewModel.Document.IsReadOnly);
            editor.Show();
            editor.Focus();
        }

        protected void HandleRawRecordEdit(object sender, RoutedEventArgs args)
        {
            args.Handled = true;
            var editor = new RawRecordEditorWindow(ViewModel.SelectedRecord);
            editor.Show();
            editor.Focus();
        }

        protected void HandleRecordContextMenuOpening(object sender, ContextMenuEventArgs args)
        {

        }
    }
}
