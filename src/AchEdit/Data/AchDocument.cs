using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using Nacho;

namespace AchEdit.Data
{
    public class AchDocument
    {
        public FileInfo File { get; set; }

        public bool IsReadOnly { get; set; }

        public ObservableCollection<AchRecord> Records { get; set; }

        public AchDocument(FileInfo file, IEnumerable<AchRecord> records)
        {
            File = file;
            IsReadOnly = false;
            Records = new ObservableCollection<AchRecord>(records);
        }
    }
}
