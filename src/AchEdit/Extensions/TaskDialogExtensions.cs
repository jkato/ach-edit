using Microsoft.WindowsAPICodePack.Dialogs;

namespace AchEdit
{
    public static class TaskDialogExtensions
    {
        public static void AttachDetailsText(this TaskDialog dialog, string text,
            string expandedLabel = "Hide Details",
            string collapsedLabel = "Show Details")
        {
            if (string.IsNullOrWhiteSpace(text))
                return;

            if (string.IsNullOrWhiteSpace(expandedLabel))
                expandedLabel = "";

            if (string.IsNullOrWhiteSpace(collapsedLabel))
                collapsedLabel = "";

            dialog.DetailsExpandedLabel = expandedLabel;
            dialog.DetailsCollapsedLabel = collapsedLabel;
            dialog.DetailsExpandedText = text;
        }
    }
}
