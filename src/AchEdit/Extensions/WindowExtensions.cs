using System.Windows;
using System.Windows.Interop;
using AchEdit.Win32;

namespace AchEdit
{
    using static User32;

    public static class WindowExtensions
    {
        public static void RemoveMaxMinButtons(this Window window)
        {
            var helper = new WindowInteropHelper(window);

            var curStyle = GetWindowLong(helper.Handle, GWL_STYLE);
            var newStyle = curStyle & ~WS_MAXIMIZEBOX & ~WS_MINIMIZEBOX;

            SetWindowLong(helper.Handle, GWL_STYLE, newStyle);
        }
    }
}
