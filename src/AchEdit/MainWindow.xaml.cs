using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Shell;
using AchEdit.Controls;
using AchEdit.Data;
using AchEdit.Providers;
using AchEdit.ViewModels;
using Nacho;
using Xceed.Wpf.AvalonDock;

namespace AchEdit
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public IntPtr Handle => InteropHelper.Handle;

        public MainWindowViewModel ViewModel => (MainWindowViewModel) DataContext;

        private IDialogsProvider Dialogs { get; }

        private IFileSystem FileSystem { get; }

        private WindowInteropHelper InteropHelper { get; }

        public MainWindow()
        {
            InitializeComponent();

            FileSystem = new FileSystem();
            InteropHelper = new WindowInteropHelper(this);

            Dialogs = new DialogsProvider();
#if DEBUG
            TabDebug.Visibility = Visibility.Visible;
#endif
        }

        protected void CanDeleteRecord(object sender, CanExecuteRoutedEventArgs args) =>
            args.CanExecute = ViewModel?.ActiveDocument?.ViewModel?.SelectedRecord != null;

        protected void HandleClose(object sender, RoutedEventArgs args)
        {
            Close();
        }

        protected void HandleDocumentClosed(object sender, DocumentClosedEventArgs args)
        {
            if (args.Document.Content is AchEditorControl doc)
            {
                ViewModel.OpenDocuments.Remove(doc);
            }
        }

        protected void HandleDragDrop(object sender, DragEventArgs args)
        {
            if (!args.Data.GetDataPresent(DataFormats.FileDrop))
                return;

            var files = (string[]) args.Data.GetData(DataFormats.FileDrop);

            if (files == null)
                return;

            OpenFiles(files.Select(p => new FileInfo(p)));
        }

        protected void HandleDelete(object sender, RoutedEventArgs args)
        {
            ViewModel.ActiveDocument?.HandleRecordDelete(sender, args);
        }

        protected void HandleNew(object sender, RoutedEventArgs args)
        {
            ViewModel.OpenDocuments.Add(new AchEditorControl());

            // Close the backstage menu if its still open
            if (Backstage.IsOpen) Backstage.IsOpen = false;

            ViewModel.ActiveDocument = ViewModel.OpenDocuments.LastOrDefault();
        }

        protected void HandleOpen(object sender, RoutedEventArgs args)
        {
            var files = Dialogs.ShowOpenAchFileDialog();
            OpenFiles(files);
        }

        protected void HandlePaste(object sender, RoutedEventArgs args)
        {

        }

        protected void HandleSave(object sender, RoutedEventArgs args)
        {
            var doc = ViewModel.ActiveDocument;
            if (doc == null)
                return;

            FileSystem.WriteAch(doc.ViewModel.AchFile, doc.ViewModel.Records, App.Config.GetFileConfig().DefaultLineEnding);
        }

        protected void HandleSaveAs(object sender, RoutedEventArgs args)
        {

        }

        #region Debug Events

        protected void HandleOpenAppDataDirectoryClick(object sender, RoutedEventArgs args)
        {
            Process.Start("explorer", App.FilePaths.AppDataDir);
        }

        #endregion

        private void OpenFiles(IEnumerable<FileInfo> files)
        {
            if (files == null)
                return;

            var filesArr = files.ToArray();
            if (filesArr.Length == 0)
                return;

            TaskbarItemInfo.ProgressState = TaskbarItemProgressState.Indeterminate;

            Task.Factory
                .StartNew(() => AddFilesToOpenDocuments(filesArr))
                .ContinueWith(SetOpenFilesComplete);
        }

        private void AddFilesToOpenDocuments(FileInfo[] files)
        {
            foreach (var file in files)
            {
                var records = FileSystem.LoadAch(file);
                var doc = new AchDocument(file, records);

                Dispatcher?.Invoke(() =>
                {
                    var editor = new AchEditorControl(doc);
                    ViewModel.OpenDocuments.Add(editor);

                    if (file == files.LastOrDefault())
                    {
                        ViewModel.ActiveDocument = editor;
                    }
                });
            }
        }

        private void SetOpenFilesComplete(Task task)
        {
            Dispatcher?.Invoke(() =>
            {
                TaskbarItemInfo.ProgressState = TaskbarItemProgressState.None;
            });
        }
    }
}
