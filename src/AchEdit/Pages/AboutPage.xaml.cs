using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for AboutPage.xaml
    /// </summary>
    public partial class AboutPage : Page
    {
        public AboutPage()
        {
            InitializeComponent();

            TxtVersion.Text = App.Version.ToString(3);
        }

        protected void HandleNavigate(object sender, RequestNavigateEventArgs args)
        {
            args.Handled = true;
            var startInfo = new ProcessStartInfo(args.Uri.AbsoluteUri)
            {
                UseShellExecute = true,
            };

            Process.Start(startInfo);
        }
    }
}
