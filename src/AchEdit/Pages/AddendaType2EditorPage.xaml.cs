using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for AddendaType2EditorPage.xaml
    /// </summary>
    public partial class AddendaType2EditorPage : Page
    {
        public AddendaType2EditorViewModel ViewModel => (AddendaType2EditorViewModel) DataContext;

        public AddendaType2EditorPage(AddendaRecordType2 record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
