using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for AddendaType5EditorPage.xaml
    /// </summary>
    public partial class AddendaType5EditorPage : Page
    {
        public AddendaType5EditorViewModel ViewModel => (AddendaType5EditorViewModel) DataContext;

        public AddendaType5EditorPage(AddendaRecordType5 record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
