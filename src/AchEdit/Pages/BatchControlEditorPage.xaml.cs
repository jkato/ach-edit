using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for BatchControlEditorPage.xaml
    /// </summary>
    public partial class BatchControlEditorPage : Page
    {
        public BatchControlEditorViewModel ViewModel => (BatchControlEditorViewModel) DataContext;

        public BatchControlEditorPage(BatchControlRecord record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
