using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for BatchHeaderEditorPage.xaml
    /// </summary>
    public partial class BatchHeaderEditorPage : Page
    {
        public BatchHeaderEditorViewModel ViewModel => (BatchHeaderEditorViewModel) DataContext;

        public BatchHeaderEditorPage(BatchHeaderRecord record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
