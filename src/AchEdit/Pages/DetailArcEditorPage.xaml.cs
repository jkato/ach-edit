using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for DetailArcEditorPage.xaml
    /// </summary>
    public partial class DetailArcEditorPage : Page
    {
        public DetailArcEditorViewModel ViewModel => (DetailArcEditorViewModel) DataContext;

        public DetailArcEditorPage(DetailRecordArc record, bool isReadOnly = true)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
