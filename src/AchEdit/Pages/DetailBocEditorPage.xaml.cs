using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for DetailBocEditorPage.xaml
    /// </summary>
    public partial class DetailBocEditorPage : Page
    {
        public DetailBocEditorViewModel ViewModel => (DetailBocEditorViewModel) DataContext;

        public DetailBocEditorPage(DetailRecordBoc record, bool isReadOnly = true)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
