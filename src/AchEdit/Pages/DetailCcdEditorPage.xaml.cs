using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for DetailCcdEditorPage.xaml
    /// </summary>
    public partial class DetailCcdEditorPage : Page
    {
        public DetailCcdEditorViewModel ViewModel => (DetailCcdEditorViewModel) DataContext;

        public DetailCcdEditorPage(DetailRecordCcd record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
