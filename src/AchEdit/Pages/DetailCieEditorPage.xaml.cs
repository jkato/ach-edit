using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for DetailCieEditorPage.xaml
    /// </summary>
    public partial class DetailCieEditorPage : Page
    {
        public DetailCieEditorViewModel ViewModel => (DetailCieEditorViewModel) DataContext;

        public DetailCieEditorPage(DetailRecordCie record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
