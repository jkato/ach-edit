using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for DetailCtxEditorPage.xaml
    /// </summary>
    public partial class DetailCtxEditorPage : Page
    {
        public DetailCtxEditorViewModel ViewModel => (DetailCtxEditorViewModel) DataContext;

        public DetailCtxEditorPage(DetailRecordCtx record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
