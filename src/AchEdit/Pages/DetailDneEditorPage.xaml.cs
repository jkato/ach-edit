using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for DetailDneEditorPage.xaml
    /// </summary>
    public partial class DetailDneEditorPage : Page
    {
        public DetailDneEditorViewModel ViewModel => (DetailDneEditorViewModel) DataContext;

        public DetailDneEditorPage(DetailRecordDne record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
