using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for DetailEnrEditorPage.xaml
    /// </summary>
    public partial class DetailEnrEditorPage : Page
    {
        public DetailEnrEditorViewModel ViewModel => (DetailEnrEditorViewModel) DataContext;

        public DetailEnrEditorPage(DetailRecordEnr record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
