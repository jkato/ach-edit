using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for DetailMteEditorPage.xaml
    /// </summary>
    public partial class DetailMteEditorPage : Page
    {
        public DetailMteEditorViewModel ViewModel => (DetailMteEditorViewModel) DataContext;

        public DetailMteEditorPage(DetailRecordMte record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
