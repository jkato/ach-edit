using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for DetailPopEditorPage.xaml
    /// </summary>
    public partial class DetailPopEditorPage : Page
    {
        public DetailPopEditorViewModel ViewModel => (DetailPopEditorViewModel) DataContext;

        public DetailPopEditorPage(DetailRecordPop record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
