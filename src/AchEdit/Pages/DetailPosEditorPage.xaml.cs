using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for DetailPosEditorPage.xaml
    /// </summary>
    public partial class DetailPosEditorPage : Page
    {
        public DetailPosEditorViewModel ViewModel => (DetailPosEditorViewModel) DataContext;

        public DetailPosEditorPage(DetailRecordPos record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
