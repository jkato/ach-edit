using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for DetailPpdEditorPage.xaml
    /// </summary>
    public partial class DetailPpdEditorPage : Page
    {
        public DetailPpdEditorViewModel ViewModel => (DetailPpdEditorViewModel) DataContext;

        public DetailPpdEditorPage(DetailRecordPpd record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
