using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for DetailRckEditorPage.xaml
    /// </summary>
    public partial class DetailRckEditorPage : Page
    {
        public DetailRckEditorViewModel ViewModel => (DetailRckEditorViewModel) DataContext;

        public DetailRckEditorPage(DetailRecordRck record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
