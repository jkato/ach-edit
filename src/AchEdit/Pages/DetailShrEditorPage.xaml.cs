using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for DetailShrEditorPage.xaml
    /// </summary>
    public partial class DetailShrEditorPage : Page
    {
        public DetailShrEditorViewModel ViewModel => (DetailShrEditorViewModel) DataContext;

        public DetailShrEditorPage(DetailRecordShr record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
