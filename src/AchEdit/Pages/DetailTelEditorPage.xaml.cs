using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for DetailTelEditorPage.xaml
    /// </summary>
    public partial class DetailTelEditorPage : Page
    {
        public DetailTelEditorViewModel ViewModel => (DetailTelEditorViewModel) DataContext;

        public DetailTelEditorPage(DetailRecordTel record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
