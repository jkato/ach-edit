using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for DetailTrcEditorPage.xaml
    /// </summary>
    public partial class DetailTrcEditorPage : Page
    {
        public DetailTrcEditorViewModel ViewModel => (DetailTrcEditorViewModel) DataContext;

        public DetailTrcEditorPage(DetailRecordTrc record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
