using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for DetailTrxEditorPage.xaml
    /// </summary>
    public partial class DetailTrxEditorPage : Page
    {
        public DetailTrxEditorViewModel ViewModel => (DetailTrxEditorViewModel) DataContext;

        public DetailTrxEditorPage(DetailRecordTrx record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
