using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for DetailWebEditorPage.xaml
    /// </summary>
    public partial class DetailWebEditorPage : Page
    {
        public DetailWebEditorViewModel ViewModel => (DetailWebEditorViewModel) DataContext;

        public DetailWebEditorPage(DetailRecordWeb record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
