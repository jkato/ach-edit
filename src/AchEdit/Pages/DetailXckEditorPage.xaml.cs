using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for DetailXckEditorPage.xaml
    /// </summary>
    public partial class DetailXckEditorPage : Page
    {
        public DetailXckEditorViewModel ViewModel => (DetailXckEditorViewModel) DataContext;

        public DetailXckEditorPage(DetailRecordXck record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
