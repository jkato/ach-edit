using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for FileControlEditorPage.xaml
    /// </summary>
    public partial class FileControlEditorPage : Page
    {
        public FileControlEditorViewModel ViewModel => (FileControlEditorViewModel) DataContext;

        public FileControlEditorPage(FileControlRecord record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
