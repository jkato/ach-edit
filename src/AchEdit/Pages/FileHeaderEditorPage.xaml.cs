using System.Windows.Controls;
using AchEdit.ViewModels;
using Nacho.Records;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for FileHeaderEditorPage.xaml
    /// </summary>
    public partial class FileHeaderEditorPage : Page
    {
        public FileHeaderEditorViewModel ViewModel => (FileHeaderEditorViewModel) DataContext;

        public FileHeaderEditorPage(FileHeaderRecord record, bool isReadOnly = false)
        {
            InitializeComponent();
            ViewModel.PopulateFrom(record);
            ViewModel.IsReadOnly = isReadOnly;
        }
    }
}
