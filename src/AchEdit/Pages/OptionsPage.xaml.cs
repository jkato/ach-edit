using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AchEdit.Configuration;
using AchEdit.Providers;
using AchEdit.ViewModels;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for OptionsPage.xaml
    /// </summary>
    public partial class OptionsPage : Page
    {
        private IConfigRepository Config { get; }

        private IDialogsProvider Dialogs { get; }

        private OptionsViewModel ViewModel => (OptionsViewModel) DataContext;

        public OptionsPage()
        {
            InitializeComponent();

            Config = App.Config;
            Dialogs = new DialogsProvider();
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            // Basically, when the user leaves the option page, we want to save the configuration back
            // to the configuration file.
            if (e.Property == IsVisibleProperty &&
                e.OldValue is bool oldValue &&
                e.NewValue is bool newValue &&
                oldValue && !newValue)
            {
                Config.Save(App.FilePaths.ConfigFile);
            }
        }

        protected void HandleOpenAppDataDirectoryClick(object sender, RoutedEventArgs args)
        {
            Process.Start("explorer", App.FilePaths.AppDataDir);
        }

        protected void HandleOpenLogsDirectoryClick(object sender, RoutedEventArgs args)
        {
            Process.Start("explorer", App.FilePaths.LogsDir);
        }

        protected void HandleTailLogFileClick(object sender, RoutedEventArgs args)
        {
            const string psArgsTemplate =
                "-Command \"" +
                "$Host.Ui.RawUi.WindowTitle = 'ACH Edit Log'; " +
                "Get-Content -Tail 10 -Wait '{0}'" +
                "\"";

            var logFile = Directory.EnumerateFiles(App.FilePaths.LogsDir, "*.log")
                .Select(p => new FileInfo(p))
                .OrderBy(f => f.CreationTime)
                .LastOrDefault();

            if (logFile != null && logFile.Exists)
            {
                var psArgs = string.Format(psArgsTemplate, logFile.FullName);
                Process.Start("powershell", psArgs);
            }
        }

        protected void HandleResetDefaultColor(object sender, RoutedEventArgs args)
        {
            args.Handled = true;
            if (!(sender is Button btn))
                return;

            switch (btn.Name)
            {
                case nameof(BtnDefaultFileHighlightColor):
                    ViewModel.RecordColors.FileRecord = RecordColorsConfig.DefaultFileRecord;
                    break;
                case nameof(BtnDefaultFBatchHighlightColor):
                    ViewModel.RecordColors.BatchRecord = RecordColorsConfig.DefaultBatchRecord;
                    break;
                case nameof(BtnDefaultDetailCreditHighlightColor):
                    ViewModel.RecordColors.DetailCredit = RecordColorsConfig.DefaultDetailCredit;
                    break;
                case nameof(BtnDefaultDetailDebitHighlightColor):
                    ViewModel.RecordColors.DetailDebit = RecordColorsConfig.DefaultDetailDebit;
                    break;
                case nameof(BtnDefaultAddendaHighlightColor):
                    ViewModel.RecordColors.Addenda = RecordColorsConfig.DefaultAddenda;
                    break;
                default:
#if DEBUG
                    Debug.WriteLine($"Cannot set default color for unrecognized button: {btn.Name}", "Options");
#endif
                    break;
            }
        }
    }
}
