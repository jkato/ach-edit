using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace AchEdit.Pages
{
    /// <summary>
    /// Interaction logic for UnsupportedRecordEditorPage.xaml
    /// </summary>
    public partial class UnsupportedRecordEditorPage : Page
    {
        public UnsupportedRecordEditorPage()
        {
            InitializeComponent();
        }

        protected void HandleNavigate(object sender, RequestNavigateEventArgs args)
        {
            args.Handled = true;
            var startInfo = new ProcessStartInfo(args.Uri.AbsoluteUri)
            {
                UseShellExecute = true,
            };

            Process.Start(startInfo);
        }
    }
}
