using System;
using System.IO;
using System.Linq;
using System.Windows;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace AchEdit.Providers
{
    public interface IDialogsProvider
    {
        TaskDialogResult ShowErrorDialog(string title, string text, string detailText = "");

        TaskDialogResult ShowErrorDialog(IntPtr owner, string title, string text, string detailText = "");

        TaskDialogResult ShowInfoDialog(string title, string text, string detailText = "");

        TaskDialogResult ShowInfoDialog(IntPtr owner, string title, string text, string detailText = "");

        FileInfo[] ShowOpenAchFileDialog();
    }

    public class DialogsProvider : IDialogsProvider
    {
        public static DialogsProvider Instance => LazyInstance.Value;

        private static Lazy<DialogsProvider> LazyInstance { get; } =
            new Lazy<DialogsProvider>(() => new DialogsProvider());

        private IntPtr MainWindowHandle { get; }

        public DialogsProvider()
        {
            MainWindowHandle = ((App) Application.Current).GetMainWindowHandle();
        }

        public TaskDialogResult ShowErrorDialog(string title, string text, string detailText = "")
        {
            return ShowErrorDialog(IntPtr.Zero, title, text, detailText);
        }

        public TaskDialogResult ShowErrorDialog(IntPtr owner, string title, string text, string detailText = "")
        {
            using var dialog = new TaskDialog
            {
                Icon = TaskDialogStandardIcon.Error,
                Caption = title,
                Text = text,
                StandardButtons = TaskDialogStandardButtons.Ok,
                HyperlinksEnabled = true,
            };

            if (owner != IntPtr.Zero)
                dialog.OwnerWindowHandle = owner;

            dialog.AttachDetailsText(detailText);

            return dialog.Show();
        }

        public TaskDialogResult ShowInfoDialog(string title, string text, string detailText = "")
        {
            return ShowInfoDialog(MainWindowHandle, title, text, detailText);
        }

        public TaskDialogResult ShowInfoDialog(IntPtr owner, string title, string text, string detailText = "")
        {
            using var dialog = new TaskDialog
            {
                Icon = TaskDialogStandardIcon.Information,
                Caption = title,
                Text = text,
                StandardButtons = TaskDialogStandardButtons.Ok,
                HyperlinksEnabled = true,
            };

            if (owner != IntPtr.Zero)
                dialog.OwnerWindowHandle = owner;

            dialog.AttachDetailsText(detailText);

            return dialog.Show();
        }

        public FileInfo[] ShowOpenAchFileDialog()
        {
            using var dialog = new CommonOpenFileDialog
            {
                Title = "Open ACH File...",
                Filters =
                {
                    new CommonFileDialogFilter("ACH Files", "*.ach"),
                    new CommonFileDialogFilter("All Files", "*.*")
                },
            };

            var result = dialog.ShowDialog();
            
            switch(result)
            {
                case CommonFileDialogResult.Ok:
                    return dialog.FileNames.Select(n => new FileInfo(n)).ToArray();
                default:
                    return null;
            }
        }
    }
}
