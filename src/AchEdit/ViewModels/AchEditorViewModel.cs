using System.Collections.ObjectModel;
using System.IO;
using AchEdit.Configuration;
using AchEdit.Data;
using Nacho;

namespace AchEdit.ViewModels
{
    public class AchEditorViewModel : BaseViewModel
    {
        public FileInfo AchFile
        {
            get => Document?.File;
            set => Document.File = value;
        }

        public AchDocument Document { get; set; }

        public bool HasChanges { get; set; }

        public bool ReadOnly { get; set; } = false;

        public ObservableCollection<AchRecord> Records
        {
            get => Document?.Records;
            set => Document.Records = value;
        }

        public AchRecord SelectedRecord { get; set; }

        public int SelectedRecordIndex { get; set; }

        public RecordColorsConfig RecordColors { get; }

        public AchEditorViewModel()
        {
            RecordColors = App.Config.GetRecordColors();
        }
    }
}
