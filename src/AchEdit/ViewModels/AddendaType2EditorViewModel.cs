using Nacho.Records;

namespace AchEdit.ViewModels
{
    public class AddendaType2EditorViewModel : TrackedViewModel, IRecordEditorViewModel<AddendaRecordType2>
    {
        public char RecordTypeCode { get; set; }

        public string AddendaTypeCode { get; set; }
        
        public string ReferenceInformation1 { get; set; }

        public string ReferenceInformation2 { get; set; }
        
        public string TerminalIdentificationCode { get; set; }
        
        public string TransactionSerialNumber { get; set; }
        
        public string TransactionDate { get; set; }
        
        public string AuthCode { get; set; }
        
        public string TerminalLocation { get; set; }
        
        public string TerminalCity { get; set; }
        
        public string TerminalState { get; set; }
        
        public string TraceNumber { get; set; }


        public void PopulateFrom(AddendaRecordType2 record)
        {
            RecordTypeCode = record.RecordTypeCode;
            AddendaTypeCode = record.AddendaTypeCode;
            ReferenceInformation1 = record.ReferenceInformation1;
            ReferenceInformation2 = record.ReferenceInformation2;
            TerminalIdentificationCode = record.TerminalIdentificationCode;
            TransactionSerialNumber = record.TransactionSerialNumber;
            TransactionDate = record.TransactionDate;
            AuthCode = record.AuthCode;
            TerminalLocation = record.TerminalLocation;
            TerminalCity = record.TerminalCity;
            TerminalState = record.TerminalState;
            TraceNumber = record.TraceNumber;
        }

        public void ApplyTo(AddendaRecordType2 record)
        {
            record.RecordTypeCode = RecordTypeCode;
            record.AddendaTypeCode = AddendaTypeCode;
            record.ReferenceInformation1 = ReferenceInformation1;
            record.ReferenceInformation2 = ReferenceInformation2;
            record.TerminalIdentificationCode = TerminalIdentificationCode;
            record.TransactionSerialNumber = TransactionSerialNumber;
            record.TransactionDate = TransactionDate;
            record.AuthCode = AuthCode;
            record.TerminalLocation = TerminalLocation;
            record.TerminalCity = TerminalCity;
            record.TerminalState = TerminalState;
            record.TraceNumber = TraceNumber;
        }
    }
}
