using Nacho.Records;

namespace AchEdit.ViewModels
{
    public class AddendaType5EditorViewModel : TrackedViewModel, IRecordEditorViewModel<AddendaRecordType5>
    {
        public char RecordTypeCode { get; set; }

        public string AddendaTypeCode { get; set; }

        public string PaymentRelatedInformation { get; set; }

        public int? AddendaSequenceNumber { get; set; }

        public int? EntryDetailSequenceNumber { get; set; }
        
        public void PopulateFrom(AddendaRecordType5 record)
        {
            RecordTypeCode = record.RecordTypeCode;
            AddendaTypeCode = record.AddendaTypeCode;
            PaymentRelatedInformation = record.PaymentRelatedInformation;
            AddendaSequenceNumber = record.AddendaSequenceNumber;
            EntryDetailSequenceNumber = record.EntryDetailSequenceNumber;
        }

        public void ApplyTo(AddendaRecordType5 record)
        {
            record.RecordTypeCode = RecordTypeCode;
            record.AddendaTypeCode = AddendaTypeCode;
            record.PaymentRelatedInformation = PaymentRelatedInformation;
            record.AddendaSequenceNumber = AddendaSequenceNumber;
            record.EntryDetailSequenceNumber = EntryDetailSequenceNumber;
        }
    }
}
