using Nacho.Records;

namespace AchEdit.ViewModels
{
    public class BatchControlEditorViewModel : TrackedViewModel, IRecordEditorViewModel<BatchControlRecord>
    {
        public char RecordTypeCode { get; set; }

        public string ServiceClassCode { get; set; }

        public int EntryAddendaCount { get; set; }

        public string EntryHash { get; set; }

        public long TotalDebitAmount { get; set; }

        public long TotalCreditAmount { get; set; }

        public string CompanyIdentification { get; set; }

        public string MessageAuthenticationCode { get; set; }

        public string Reserved { get; set; }

        public string OriginatingDfiIdentification { get; set; }

        public int BatchNumber { get; set; }

        public BatchControlEditorViewModel()
        { }

        public BatchControlEditorViewModel(BatchControlRecord record)
        {
            PopulateFrom(record);
        }

        public void PopulateFrom(BatchControlRecord record)
        {
            RecordTypeCode = record.RecordTypeCode;
            ServiceClassCode = record.ServiceClassCode;
            EntryAddendaCount = record.EntryAddendaCount;
            EntryHash = record.EntryHash;
            TotalDebitAmount = record.TotalDebitAmount;
            TotalCreditAmount = record.TotalCreditAmount;
            CompanyIdentification = record.CompanyIdentification;
            MessageAuthenticationCode = record.MessageAuthenticationCode;
            Reserved = record.Reserved;
            OriginatingDfiIdentification = record.OriginatingDfiIdentification;
            BatchNumber = record.BatchNumber;
        }

        public void ApplyTo(BatchControlRecord record)
        {
            record.RecordTypeCode = RecordTypeCode;
            record.ServiceClassCode = ServiceClassCode;
            record.EntryAddendaCount = EntryAddendaCount;
            record.EntryHash = EntryHash;
            record.TotalDebitAmount = TotalDebitAmount;
            record.TotalCreditAmount = TotalCreditAmount;
            record.CompanyIdentification = CompanyIdentification;
            record.MessageAuthenticationCode = MessageAuthenticationCode;
            record.Reserved = Reserved;
            record.OriginatingDfiIdentification = OriginatingDfiIdentification;
            record.BatchNumber = BatchNumber;
        }
    }
}