using System;
using Nacho.Records;

namespace AchEdit.ViewModels
{
    public class BatchHeaderEditorViewModel : TrackedViewModel, IRecordEditorViewModel<BatchHeaderRecord>
    {
        public char RecordTypeCode { get; set; }

        public string ServiceClassCode { get; set; }

        public string CompanyName { get; set; }

        public string CompanyDiscretionaryData { get; set; }

        public string CompanyIdentification { get; set; }

        public string EntryClassCode { get; set; }

        public string CompanyEntryDescription { get; set; }

        public string CompanyDescriptiveDate { get; set; }

        public DateTime EffectiveEntryDate { get; set; }

        public int? SettlementDate { get; set; }

        public char OriginatorStatusCode { get; set; }

        public string OriginatingDfiIdentification { get; set; }

        public int? BatchNumber { get; set; }

        public BatchHeaderEditorViewModel()
        { }

        public BatchHeaderEditorViewModel(BatchHeaderRecord record)
        {
            PopulateFrom(record);
        }

        public void PopulateFrom(BatchHeaderRecord record)
        {
            RecordTypeCode = record.RecordTypeCode;
            ServiceClassCode = record.ServiceClassCode;
            CompanyName = record.CompanyName;
            CompanyDiscretionaryData = record.CompanyDiscretionaryData;
            CompanyIdentification = record.CompanyIdentification;
            EntryClassCode = record.EntryClassCode;
            CompanyEntryDescription = record.CompanyEntryDescription;
            CompanyDescriptiveDate = record.CompanyDescriptiveDate;
            EffectiveEntryDate = record.EffectiveEntryDate ?? new DateTime(1970, 1, 1);
            SettlementDate = record.SettlementDate;
            OriginatorStatusCode = record.OriginatorStatusCode;
            OriginatingDfiIdentification = record.OriginatingDfiIdentification;
            BatchNumber = record.BatchNumber;
        }

        public void ApplyTo(BatchHeaderRecord record)
        {
            record.RecordTypeCode = RecordTypeCode;
            record.ServiceClassCode = ServiceClassCode;
            record.CompanyName = CompanyName;
            record.CompanyDiscretionaryData = CompanyDiscretionaryData;
            record.CompanyIdentification = CompanyIdentification;
            record.EntryClassCode = EntryClassCode;
            record.CompanyEntryDescription = CompanyEntryDescription;
            record.CompanyDescriptiveDate = CompanyDescriptiveDate;
            record.EffectiveEntryDate = EffectiveEntryDate;
            record.SettlementDate = SettlementDate;
            record.OriginatorStatusCode = OriginatorStatusCode;
            record.OriginatingDfiIdentification = OriginatingDfiIdentification;
            record.BatchNumber = BatchNumber;
        }
    }
}
