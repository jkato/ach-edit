using Nacho.Records;

namespace AchEdit.ViewModels
{
    public class DetailArcEditorViewModel : TrackedViewModel, IRecordEditorViewModel<DetailRecordArc>
    {
        public char RecordTypeCode { get; set; }
        
        public string TransactionCode { get; set; }
        
        public string ReceivingDfiIdentification { get; set; }
        
        public char CheckDigit { get; set; }
        
        public string ReceivingRoutingNumber { get; set; }
        
        public string DfiAccountNumber { get; set; }
        
        public long Amount { get; set; }
        
        public string CheckSerialNumber { get; set; }
        
        public string Name { get; set; }
        
        public string DiscretionaryData { get; set; }
        
        public char AddendaRecordIndicator { get; set; }
        
        public string TraceNumber { get; set; }

        public DetailArcEditorViewModel()
        { }

        public DetailArcEditorViewModel(DetailRecordArc record)
        {
            PopulateFrom(record);
        }

        public void PopulateFrom(DetailRecordArc record)
        {
            RecordTypeCode = record.RecordTypeCode;
            TransactionCode = record.TransactionCode;
            ReceivingDfiIdentification = record.ReceivingDfiIdentification;
            CheckDigit = record.CheckDigit;
            ReceivingRoutingNumber = record.ReceivingRoutingNumber;
            DfiAccountNumber = record.DfiAccountNumber;
            Amount = record.Amount;
            CheckSerialNumber = record.CheckSerialNumber;
            Name = record.Name;
            DiscretionaryData = record.DiscretionaryData;
            AddendaRecordIndicator = record.AddendaRecordIndicator;
            TraceNumber = record.TraceNumber;
        }

        public void ApplyTo(DetailRecordArc record)
        {
            record.RecordTypeCode = RecordTypeCode;
            record.TransactionCode = TransactionCode;
            record.ReceivingDfiIdentification = ReceivingDfiIdentification;
            record.CheckDigit = CheckDigit;
            record.ReceivingRoutingNumber = ReceivingRoutingNumber;
            record.DfiAccountNumber = DfiAccountNumber;
            record.Amount = Amount;
            record.CheckSerialNumber = CheckSerialNumber;
            record.Name = Name;
            record.DiscretionaryData = DiscretionaryData;
            record.AddendaRecordIndicator = AddendaRecordIndicator;
            record.TraceNumber = TraceNumber;
        }
    }
}
