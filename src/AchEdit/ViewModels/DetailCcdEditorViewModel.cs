using Nacho.Records;

namespace AchEdit.ViewModels
{
    public class DetailCcdEditorViewModel : TrackedViewModel, IRecordEditorViewModel<DetailRecordCcd>
    {
        public char RecordTypeCode { get; set; }

        public string TransactionCode { get; set; }

        public string ReceivingDfiIdentification { get; set; }

        public char CheckDigit { get; set; }

        public string DfiAccountNumber { get; set; }

        public long? Amount { get; set; }

        public string IdentificationNumber { get; set; }

        public string ReceivingCompanyName { get; set; }

        public string DiscretionaryData { get; set; }

        public char AddendaRecordIndicator { get; set; }

        public string TraceNumber { get; set; }

        public DetailCcdEditorViewModel()
        { }

        public DetailCcdEditorViewModel(DetailRecordCcd record)
        {
            PopulateFrom(record);
        }

        public void PopulateFrom(DetailRecordCcd record)
        {
            RecordTypeCode = record.RecordTypeCode;
            TransactionCode = record.TransactionCode;
            ReceivingDfiIdentification = record.ReceivingDfiIdentification;
            CheckDigit = record.CheckDigit;
            DfiAccountNumber = record.DfiAccountNumber;
            Amount = record.Amount;
            IdentificationNumber = record.IdentificationNumber;
            ReceivingCompanyName = record.ReceivingCompanyName;
            DiscretionaryData = record.DiscretionaryData;
            AddendaRecordIndicator = record.AddendaRecordIndicator;
            TraceNumber = record.TraceNumber;
        }

        public void ApplyTo(DetailRecordCcd record)
        {
            record.RecordTypeCode = RecordTypeCode;
            record.TransactionCode = TransactionCode;
            record.ReceivingDfiIdentification = ReceivingDfiIdentification;
            record.CheckDigit = CheckDigit;
            record.DfiAccountNumber = DfiAccountNumber;
            record.Amount = Amount;
            record.IdentificationNumber = IdentificationNumber;
            record.ReceivingCompanyName = ReceivingCompanyName;
            record.DiscretionaryData = DiscretionaryData;
            record.AddendaRecordIndicator = AddendaRecordIndicator;
            record.TraceNumber = TraceNumber;
        }
    }
}
