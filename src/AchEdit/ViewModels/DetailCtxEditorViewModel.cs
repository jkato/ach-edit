using Nacho.Records;

namespace AchEdit.ViewModels
{
    public class DetailCtxEditorViewModel : TrackedViewModel, IRecordEditorViewModel<DetailRecordCtx>
    {
        public char RecordTypeCode { get; set; }

        public string TransactionCode { get; set; }

        public string ReceivingDfiIdentification { get; set; }

        public char CheckDigit { get; set; }

        public string ReceivingRoutingNumber { get; set; }

        public string DfiAccountNumber { get; set; }

        public long? Amount { get; set; }

        public string IdentificationNumber { get; set; }

        public int NumberOfAddendaRecords { get; set; }

        public string ReceivingCompanyName { get; set; }

        public string Reserved { get; set; }

        public string DiscretionaryData { get; set; }

        public char AddendaRecordIndicator { get; set; }

        public string TraceNumber { get; set; }

        public DetailCtxEditorViewModel()
        { }

        public DetailCtxEditorViewModel(DetailRecordCtx record)
        {
            PopulateFrom(record);
        }

        public void PopulateFrom(DetailRecordCtx record)
        {
            RecordTypeCode = record.RecordTypeCode;
            TransactionCode = record.TransactionCode;
            ReceivingDfiIdentification = record.ReceivingDfiIdentification;
            CheckDigit = record.CheckDigit;
            ReceivingRoutingNumber = record.ReceivingRoutingNumber;
            DfiAccountNumber = record.DfiAccountNumber;
            Amount = record.Amount;
            IdentificationNumber = record.IdentificationNumber;
            NumberOfAddendaRecords = record.NumberOfAddendaRecords;
            ReceivingCompanyName = record.ReceivingCompanyName;
            Reserved = record.Reserved;
            DiscretionaryData = record.DiscretionaryData;
            AddendaRecordIndicator = record.AddendaRecordIndicator;
            TraceNumber = record.TraceNumber;
        }

        public void ApplyTo(DetailRecordCtx record)
        {
            record.RecordTypeCode = RecordTypeCode;
            record.TransactionCode = TransactionCode;
            record.ReceivingDfiIdentification = ReceivingDfiIdentification;
            record.CheckDigit = CheckDigit;
            record.ReceivingRoutingNumber = ReceivingRoutingNumber;
            record.DfiAccountNumber = DfiAccountNumber;
            record.Amount = Amount;
            record.IdentificationNumber = IdentificationNumber;
            record.NumberOfAddendaRecords = NumberOfAddendaRecords;
            record.ReceivingCompanyName = ReceivingCompanyName;
            record.Reserved = Reserved;
            record.DiscretionaryData = DiscretionaryData;
            record.AddendaRecordIndicator = AddendaRecordIndicator;
            record.TraceNumber = TraceNumber;
        }
    }
}
