using Nacho.Records;

namespace AchEdit.ViewModels
{
    public class DetailDneEditorViewModel : TrackedViewModel, IRecordEditorViewModel<DetailRecordDne>
    {
        public char RecordTypeCode { get; set; }
        
        public string TransactionCode { get; set; }
        
        public string ReceivingDfiIdentification { get; set; }
        
        public char CheckDigit { get; set; }
        
        public string ReceivingRoutingNumber { get; set; }
        
        public string DfiAccountNumber { get; set; }
        
        public long Amount { get; set; }
        
        public string IndividualIdentificationNumber { get; set; }
        
        public string IndividualName { get; set; }
        
        public string DiscretionaryData { get; set; }
        
        public char AddendaRecordIndicator { get; set; }
        
        public string TraceNumber { get; set; }

        public DetailDneEditorViewModel()
        { }

        public DetailDneEditorViewModel(DetailRecordDne record)
        {
            PopulateFrom(record);
        }

        public void PopulateFrom(DetailRecordDne record)
        {
            RecordTypeCode = record.RecordTypeCode;
            TransactionCode = record.TransactionCode;
            ReceivingDfiIdentification = record.ReceivingDfiIdentification;
            CheckDigit = record.CheckDigit;
            ReceivingRoutingNumber = record.ReceivingRoutingNumber;
            DfiAccountNumber = record.DfiAccountNumber;
            Amount = record.Amount;
            IndividualIdentificationNumber = record.IndividualIdentificationNumber;
            IndividualName = record.IndividualName;
            DiscretionaryData = record.DiscretionaryData;
            AddendaRecordIndicator = record.AddendaRecordIndicator;
            TraceNumber = record.TraceNumber;
        }

        public void ApplyTo(DetailRecordDne record)
        {
            record.RecordTypeCode = RecordTypeCode;
            record.TransactionCode = TransactionCode;
            record.ReceivingDfiIdentification = ReceivingDfiIdentification;
            record.CheckDigit = CheckDigit;
            record.ReceivingRoutingNumber = ReceivingRoutingNumber;
            record.DfiAccountNumber = DfiAccountNumber;
            record.Amount = Amount;
            record.IndividualIdentificationNumber = IndividualIdentificationNumber;
            record.IndividualName = IndividualName;
            record.DiscretionaryData = DiscretionaryData;
            record.AddendaRecordIndicator = AddendaRecordIndicator;
            record.TraceNumber = TraceNumber;
        }
    }
}
