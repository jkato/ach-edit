using Nacho.Records;

namespace AchEdit.ViewModels
{
    public class DetailPosEditorViewModel : TrackedViewModel, IRecordEditorViewModel<DetailRecordPos>
    {
        public char RecordTypeCode { get; set; }

        public string TransactionCode { get; set; }

        public string ReceivingDfiIdentification { get; set; }

        public char CheckDigit { get; set; }

        public string DfiAccountNumber { get; set; }

        public long? Amount { get; set; }

        public string IndividualIdentificationNumber { get; set; }

        public string IndividualName { get; set; }

        public string CardTransactionTypeCode { get; set; }

        public char AddendaRecordIndicator { get; set; }

        public string TraceNumber { get; set; }

        public DetailPosEditorViewModel()
        { }

        public DetailPosEditorViewModel(DetailRecordPos record)
        {
            PopulateFrom(record);
        }

        public void PopulateFrom(DetailRecordPos record)
        {
            RecordTypeCode = record.RecordTypeCode;
            TransactionCode = record.TransactionCode;
            ReceivingDfiIdentification = record.ReceivingDfiIdentification;
            CheckDigit = record.CheckDigit;
            DfiAccountNumber = record.DfiAccountNumber;
            Amount = record.Amount;
            IndividualIdentificationNumber = record.IndividualIdentificationNumber;
            IndividualName = record.IndividualName;
            CardTransactionTypeCode = record.CardTransactionTypeCode;
            AddendaRecordIndicator = record.AddendaRecordIndicator;
            TraceNumber = record.TraceNumber;
        }

        public void ApplyTo(DetailRecordPos record)
        {
            record.RecordTypeCode = RecordTypeCode;
            record.TransactionCode = TransactionCode;
            record.ReceivingDfiIdentification = ReceivingDfiIdentification;
            record.CheckDigit = CheckDigit;
            record.DfiAccountNumber = DfiAccountNumber;
            record.Amount = Amount;
            record.IndividualIdentificationNumber = IndividualIdentificationNumber;
            record.IndividualName = IndividualName;
            record.CardTransactionTypeCode = CardTransactionTypeCode;
            record.AddendaRecordIndicator = AddendaRecordIndicator;
            record.TraceNumber = TraceNumber;
        }
    }
}
