using Nacho.Records;

namespace AchEdit.ViewModels
{
    public class DetailPpdEditorViewModel : TrackedViewModel, IRecordEditorViewModel<DetailRecordPpd>
    {
        public char RecordTypeCode { get; set; }

        public string TransactionCode { get; set; }

        public string ReceivingDfiIdentification { get; set; }

        public char CheckDigit { get; set; }

        public string DfiAccountNumber { get; set; }

        public long? Amount { get; set; }

        public string IndividualIdentificationNumber { get; set; }

        public string IndividualName { get; set; }

        public string PaymentTypeCode { get; set; }

        public char AddendaRecordIndicator { get; set; }

        public string TraceNumber { get; set; }

        public DetailPpdEditorViewModel()
        { }

        public DetailPpdEditorViewModel(DetailRecordPpd record)
        {
            PopulateFrom(record);
        }

        public void PopulateFrom(DetailRecordPpd record)
        {
            RecordTypeCode = record.RecordTypeCode;
            TransactionCode = record.TransactionCode;
            ReceivingDfiIdentification = record.ReceivingDfiIdentification;
            CheckDigit = record.CheckDigit;
            DfiAccountNumber = record.DfiAccountNumber;
            Amount = record.Amount;
            IndividualIdentificationNumber = record.IndividualIdentificationNumber;
            IndividualName = record.IndividualName;
            PaymentTypeCode = record.PaymentTypeCode;
            AddendaRecordIndicator = record.AddendaRecordIndicator;
            TraceNumber = record.TraceNumber;
        }

        public void ApplyTo(DetailRecordPpd record)
        {
            record.RecordTypeCode = RecordTypeCode;
            record.TransactionCode = TransactionCode;
            record.ReceivingDfiIdentification = ReceivingDfiIdentification;
            record.CheckDigit = CheckDigit;
            record.DfiAccountNumber = DfiAccountNumber;
            record.Amount = Amount;
            record.IndividualIdentificationNumber = IndividualIdentificationNumber;
            record.IndividualName = IndividualName;
            record.PaymentTypeCode = PaymentTypeCode;
            record.AddendaRecordIndicator = AddendaRecordIndicator;
            record.TraceNumber = TraceNumber;
        }
    }
}
