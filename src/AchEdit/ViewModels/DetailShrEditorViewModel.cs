using Nacho.Records;

namespace AchEdit.ViewModels
{
    public class DetailShrEditorViewModel : TrackedViewModel, IRecordEditorViewModel<DetailRecordShr>
    {
        public char RecordTypeCode { get; set; }

        public string TransactionCode { get; set; }
        
        public string ReceivingDfiIdentification { get; set; }
        
        public char CheckDigit { get; set; }
        
        public string ReceivingRoutingNumber { get; set; }
        
        public string DfiAccountNumber { get; set; }
        
        public long? Amount { get; set; }
        
        public string CardExpDate { get; set; }
        
        public string DocumentReferenceNumber { get; set; }
        
        public string IndividualCardAccountNumber { get; set; }
        
        public string CardTransactionTypeCode { get; set; }
        
        public char AddendaRecordIndicator { get; set; }
        
        public string TraceNumber { get; set; }

        public DetailShrEditorViewModel()
        { }

        public DetailShrEditorViewModel(DetailRecordShr record)
        {
            PopulateFrom(record);
        }

        public void PopulateFrom(DetailRecordShr record)
        {
            RecordTypeCode = record.RecordTypeCode;
            TransactionCode = record.TransactionCode;
            ReceivingDfiIdentification = record.ReceivingDfiIdentification;
            CheckDigit = record.CheckDigit;
            ReceivingRoutingNumber = record.ReceivingRoutingNumber;
            DfiAccountNumber = record.DfiAccountNumber;
            Amount = record.Amount;
            CardExpDate = record.CardExpDate;
            DocumentReferenceNumber = record.DocumentReferenceNumber;
            IndividualCardAccountNumber = record.IndividualCardAccountNumber;
            CardTransactionTypeCode = record.CardTransactionTypeCode;
            AddendaRecordIndicator = record.AddendaRecordIndicator;
            TraceNumber = record.TraceNumber;
        }

        public void ApplyTo(DetailRecordShr record)
        {
            record.RecordTypeCode = RecordTypeCode;
            record.TransactionCode = TransactionCode;
            record.ReceivingDfiIdentification = ReceivingDfiIdentification;
            record.CheckDigit = CheckDigit;
            record.ReceivingRoutingNumber = ReceivingRoutingNumber;
            record.DfiAccountNumber = DfiAccountNumber;
            record.Amount = Amount;
            record.CardExpDate = CardExpDate;
            record.DocumentReferenceNumber = DocumentReferenceNumber;
            record.IndividualCardAccountNumber = IndividualCardAccountNumber;
            record.CardTransactionTypeCode = CardTransactionTypeCode;
            record.AddendaRecordIndicator = AddendaRecordIndicator;
            record.TraceNumber = TraceNumber;
        }
    }
}
