using Nacho.Records;

namespace AchEdit.ViewModels
{
    public class DetailTrcEditorViewModel : TrackedViewModel, IRecordEditorViewModel<DetailRecordTrc>
    {
        public char RecordTypeCode { get; set; }
        
        public string TransactionCode { get; set; }
        
        public string ReceivingDfiIdentification { get; set; }
        
        public char CheckDigit { get; set; }
        
        public string ReceivingRoutingNumber { get; set; }
        
        public string DfiAccountNumber { get; set; }
        
        public long? Amount { get; set; }
        
        public string CheckSerialNumber { get; set; }
        
        public string ProcessControlField { get; set; }
        
        public string ItemResearchNumber { get; set; }
        
        public string ItemTypeIndicator { get; set; }
        
        public char AddendaRecordIndicator { get; set; }
        
        public string TraceNumber { get; set; }

        public void PopulateFrom(DetailRecordTrc record)
        {
            RecordTypeCode = record.RecordTypeCode;
            TransactionCode = record.TransactionCode;
            ReceivingDfiIdentification = record.ReceivingDfiIdentification;
            CheckDigit = record.CheckDigit;
            ReceivingRoutingNumber = record.ReceivingRoutingNumber;
            DfiAccountNumber = record.DfiAccountNumber;
            Amount = record.Amount;
            CheckSerialNumber = record.CheckSerialNumber;
            ProcessControlField = record.ProcessControlField;
            ItemResearchNumber = record.ItemResearchNumber;
            ItemTypeIndicator = record.ItemTypeIndicator;
            AddendaRecordIndicator = record.AddendaRecordIndicator;
            TraceNumber = record.TraceNumber;
        }

        public void ApplyTo(DetailRecordTrc record)
        {
            record.RecordTypeCode = RecordTypeCode;
            record.TransactionCode = TransactionCode;
            record.ReceivingDfiIdentification = ReceivingDfiIdentification;
            record.CheckDigit = CheckDigit;
            record.ReceivingRoutingNumber = ReceivingRoutingNumber;
            record.DfiAccountNumber = DfiAccountNumber;
            record.Amount = Amount;
            record.CheckSerialNumber = CheckSerialNumber;
            record.ProcessControlField = ProcessControlField;
            record.ItemResearchNumber = ItemResearchNumber;
            record.ItemTypeIndicator = ItemTypeIndicator;
            record.AddendaRecordIndicator = AddendaRecordIndicator;
            record.TraceNumber = TraceNumber;
        }
    }
}
