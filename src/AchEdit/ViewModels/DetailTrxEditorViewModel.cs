using Nacho.Records;

namespace AchEdit.ViewModels
{
    public class DetailTrxEditorViewModel : TrackedViewModel, IRecordEditorViewModel<DetailRecordTrx>
    {
        public char RecordTypeCode { get; set; }

        public string TransactionCode { get; set; }
        
        public string ReceivingDfiIdentification { get; set; }
        
        public char CheckDigit { get; set; }
        
        public string ReceivingRoutingNumber { get; set; }
        
        public string DfiAccountNumber { get; set; }
        
        public long? Amount { get; set; }
        
        public string IdentificationNumber { get; set; }
        
        public int NumberOfAddendaRecords { get; set; }
        
        public string ReceivingCompanyName { get; set; }
        
        public string Reserved { get; set; }
        
        public string ItemTypeIndicator { get; set; }
        
        public char AddendaRecordIndicator { get; set; }
        
        public string TraceNumber { get; set; }

        public DetailTrxEditorViewModel()
        { }

        public DetailTrxEditorViewModel(DetailRecordTrx record)
        {
            PopulateFrom(record);
        }

        public void PopulateFrom(DetailRecordTrx record)
        {
            TransactionCode = record.TransactionCode;
            ReceivingDfiIdentification = record.ReceivingDfiIdentification;
            CheckDigit = record.CheckDigit;
            ReceivingRoutingNumber = record.ReceivingRoutingNumber;
            DfiAccountNumber = record.DfiAccountNumber;
            Amount = record.Amount;
            IdentificationNumber = record.IdentificationNumber;
            NumberOfAddendaRecords = record.NumberOfAddendaRecords;
            ReceivingCompanyName = record.ReceivingCompanyName;
            Reserved = record.Reserved;
            ItemTypeIndicator = record.ItemTypeIndicator;
            AddendaRecordIndicator = record.AddendaRecordIndicator;
            TraceNumber = record.TraceNumber;
        }

        public void ApplyTo(DetailRecordTrx record)
        {
            record.TransactionCode = TransactionCode;
            record.ReceivingDfiIdentification = ReceivingDfiIdentification;
            record.CheckDigit = CheckDigit;
            record.ReceivingRoutingNumber = ReceivingRoutingNumber;
            record.DfiAccountNumber = DfiAccountNumber;
            record.Amount = Amount;
            record.IdentificationNumber = IdentificationNumber;
            record.NumberOfAddendaRecords = NumberOfAddendaRecords;
            record.ReceivingCompanyName = ReceivingCompanyName;
            record.Reserved = Reserved;
            record.ItemTypeIndicator = ItemTypeIndicator;
            record.AddendaRecordIndicator = AddendaRecordIndicator;
            record.TraceNumber = TraceNumber;
        }
    }
}
