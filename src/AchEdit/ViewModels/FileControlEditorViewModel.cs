using Nacho.Records;

namespace AchEdit.ViewModels
{
    public class FileControlEditorViewModel : TrackedViewModel, IRecordEditorViewModel<FileControlRecord>
    {
        public char RecordTypeCode { get; set; }

        public int BatchCount { get; set; }

        public int BlockCount { get; set; }

        public int EntryAddendaCount { get; set; }

        public string EntryHash { get; set; }

        public long TotalDebitAmount { get; set; }

        public long TotalCreditAmount { get; set; }

        public string Reserved { get; set; }

        public FileControlEditorViewModel()
        { }

        public FileControlEditorViewModel(FileControlRecord record)
        {
            PopulateFrom(record);
        }

        public void PopulateFrom(FileControlRecord record)
        {
            RecordTypeCode = record.RecordTypeCode;
            BatchCount = record.BatchCount;
            BlockCount = record.BlockCount;
            EntryAddendaCount = record.EntryAddendaCount;
            EntryHash = record.EntryHash;
            TotalDebitAmount = record.TotalDebitAmount;
            TotalCreditAmount = record.TotalCreditAmount;
            Reserved = record.Reserved;
        }

        public void ApplyTo(FileControlRecord record)
        {
            record.RecordTypeCode = RecordTypeCode;
            record.BatchCount = BatchCount;
            record.BlockCount = BlockCount;
            record.EntryAddendaCount = EntryAddendaCount;
            record.EntryHash = EntryHash;
            record.TotalDebitAmount = TotalDebitAmount;
            record.TotalCreditAmount = TotalCreditAmount;
            record.Reserved = Reserved;
        }
    }
}
