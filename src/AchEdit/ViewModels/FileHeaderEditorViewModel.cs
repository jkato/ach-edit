using System;
using Nacho.Records;

namespace AchEdit.ViewModels
{
    public class FileHeaderEditorViewModel : TrackedViewModel, IRecordEditorViewModel<FileHeaderRecord>
    {
        public char RecordTypeCode { get; set; }

        public int? PriorityCode { get; set; }

        public string ImmediateDestination { get; set; }

        public string ImmediateOrigin { get; set; }

        public DateTime? FileCreationDateTime { get; set; }

        public char FileIdModifier { get; set; }

        public int? RecordSize { get; set; }

        public int? BlockingFactor { get; set; }

        public char FormatCode { get; set; }

        public string ImmediateDestinationName { get; set; }

        public string ImmediateOriginName { get; set; }

        public string ReferenceCode { get; set; }

        public FileHeaderEditorViewModel()
        { }

        public FileHeaderEditorViewModel(FileHeaderRecord record)
        {
            PopulateFrom(record);
        }

        public void PopulateFrom(FileHeaderRecord record)
        {
            RecordTypeCode = record.RecordTypeCode;
            PriorityCode = record.PriorityCode;
            ImmediateDestination = record.ImmediateDestination;
            ImmediateOrigin = record.ImmediateOrigin;
            FileCreationDateTime = record.FileCreationDateTime;
            FileIdModifier = record.FileIdModifier;
            RecordSize = record.RecordSize;
            BlockingFactor = record.BlockingFactor;
            FormatCode = record.FormatCode;
            ImmediateDestinationName = record.ImmediateDestinationName;
            ImmediateOriginName = record.ImmediateOriginName;
            ReferenceCode = record.ReferenceCode;
        }

        public void ApplyTo(FileHeaderRecord record)
        {
            record.RecordTypeCode = RecordTypeCode;
            record.PriorityCode = PriorityCode;
            record.ImmediateDestination = ImmediateDestination;
            record.ImmediateOrigin = ImmediateOrigin;
            record.FileCreationDateTime = FileCreationDateTime;
            record.FileIdModifier = FileIdModifier;
            record.RecordSize = RecordSize;
            record.BlockingFactor = BlockingFactor;
            record.FormatCode = FormatCode;
            record.ImmediateDestinationName = ImmediateDestinationName;
            record.ImmediateOriginName = ImmediateOriginName;
            record.ReferenceCode = ReferenceCode;
        }
    }
}