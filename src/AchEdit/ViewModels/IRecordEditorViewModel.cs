using Nacho;

namespace AchEdit.ViewModels
{
    public interface IRecordEditorViewModel<in T>
        where T : AchRecord
    {
        void PopulateFrom(T record);

        void ApplyTo(T record);
    }
}
