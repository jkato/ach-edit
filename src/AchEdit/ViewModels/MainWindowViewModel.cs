using System.Collections.ObjectModel;
using AchEdit.Controls;

namespace AchEdit.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        public AchEditorControl ActiveDocument { get; set; }

        public ObservableCollection<AchEditorControl> OpenDocuments { get; } =
            new ObservableCollection<AchEditorControl>();
    }
}
