using System.Collections.ObjectModel;
using System.Windows.Media;
using AchEdit.Configuration;
using Xceed.Wpf.Toolkit;

namespace AchEdit.ViewModels
{
    public class OptionsViewModel : BaseViewModel
    {
        public FileConfig FileConfig { get; }

        public RecordColorsConfig RecordColors { get; }

        public ObservableCollection<ColorItem> AvailableColors { get; } =
            new ObservableCollection<ColorItem>
            {
                new ColorItem(Colors.Black, "Black"),
                new ColorItem(Colors.White, "White"),
                new ColorItem(Colors.LightBlue, "File Record Highlight"),
                new ColorItem(Colors.LightSkyBlue, "Batch Record Highlight"),
                new ColorItem(Colors.LightGreen, "Credit Detail Record Highlight"),
                new ColorItem(Colors.LightCoral, "Debit Detail Record Highlight"),
                new ColorItem(Colors.DarkGray, "Addenda Record Highlight"),
            };

        public OptionsViewModel()
        {
            FileConfig = App.Config.GetFileConfig();
            RecordColors = App.Config.GetRecordColors();
        }
    }
}
