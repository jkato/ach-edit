using Nacho;

namespace AchEdit.ViewModels
{
    public class RawRecordEditorViewModel : BaseViewModel
    {
        public int CaretIndex { get; set; }

        public string RecordData { get; set; }

        public int RecordSelectionStart { get; set; }

        public int RecordSelectionLength { get; set; }

        public void SetRecord(AchRecord record)
        {
            RecordData = record.ToString();
        }
    }
}
