using System.Windows.Controls;
using Nacho;

namespace AchEdit.ViewModels
{
    public class RecordEditorViewModel : BaseViewModel
    {
        public Page EditorPage { get; set; }

        public AchRecord Record { get; set; }
    }
}
