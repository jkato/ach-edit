using System.Collections.Concurrent;
using System.ComponentModel;

namespace AchEdit.ViewModels
{
    using ChangeLog = ConcurrentDictionary<string, (object Original, object Updated)>;

    public abstract class TrackedViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected ChangeLog Changes { get; } = new ChangeLog();

        public bool HasChanges => Changes.Count > 0;

        public bool IsReadOnly { get; set; } = false;

        protected void OnPropertyChanged(string propertyName, object before, object after)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

            if (!Changes.ContainsKey(propertyName))
            {
                Changes.TryAdd(propertyName, (before, after));
                return;
            }

            Changes.AddOrUpdate(propertyName, (before, after), DiffChangeValue);
        }

        private (object Original, object Updated) DiffChangeValue(string propName, (object Original, object Updated) change)
        {
            return change;
        }
    }
}
