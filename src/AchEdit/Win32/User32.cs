using System;
using System.Runtime.InteropServices;

namespace AchEdit.Win32
{
    /// <remarks>
    /// Based on StackOverflow answer: https://stackoverflow.com/a/339635
    /// </remarks>
    internal class User32
    {
        public const int GWL_STYLE      = -16;
        public const int WS_MAXIMIZEBOX = 0x10000;
        public const int WS_MINIMIZEBOX = 0x20000;

        [DllImport("user32.dll", EntryPoint = "GetWindowLong", SetLastError = true)]
        public static extern int GetWindowLong(IntPtr hWnd, int index);

        [DllImport("user32.dll", EntryPoint = "SetWindowLong", SetLastError = true)]
        public static extern int SetWindowLong(IntPtr hWnd, int index, int value);
    }
}
