using System;
using System.Windows;
using AchEdit.ViewModels;

namespace AchEdit.Windows
{
    /// <summary>
    /// Interaction logic for GoToRecordWindow.xaml
    /// </summary>
    public partial class GoToRecordWindow : Window
    {
        public int RecordIndex => Math.Max((ViewModel.RecordNumber ?? 0) - 1, 0);

        private GoToRecordViewModel ViewModel => (GoToRecordViewModel) DataContext;

        public GoToRecordWindow()
        {
            InitializeComponent();
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            this.RemoveMaxMinButtons();
        }

        protected void HandleSubmitRecordIndex(object sender, RoutedEventArgs args)
        {
            if (!ViewModel.RecordNumber.HasValue || ViewModel.RecordNumber.Value < 0)
                return;

            DialogResult = true;
            Close();
        }

        protected void HandleCancelClick(object sender, RoutedEventArgs args)
        {
            DialogResult = false;
            Close();
        }
    }

    public class GoToRecordViewModel : BaseViewModel
    {
        public int? RecordNumber { get; set; }
    }
}
