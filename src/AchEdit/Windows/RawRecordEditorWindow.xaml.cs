using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using AchEdit.ViewModels;
using Nacho;

namespace AchEdit.Windows
{
    /// <summary>
    /// Interaction logic for RawRecordEditorWindow.xaml
    /// </summary>
    public partial class RawRecordEditorWindow : Window
    {
        private RawRecordEditorViewModel ViewModel => (RawRecordEditorViewModel) DataContext;

        public RawRecordEditorWindow(AchRecord record)
        {
            InitializeComponent();
            ViewModel.SetRecord(record);
        }

        protected void HandleClose(object sender, RoutedEventArgs args)
        {
            Close();
        }

        protected void HandleSave(object sender, RoutedEventArgs args)
        {

        }

        protected void HandleTxtRecordQueryCursor(object sender, QueryCursorEventArgs args)
        {
            Debug.WriteLine($"{TxtRecord.CaretIndex}", "Raw Editor");
        }
    }
}
