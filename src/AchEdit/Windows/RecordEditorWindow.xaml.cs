using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using AchEdit.Pages;
using AchEdit.Providers;
using AchEdit.ViewModels;
using Nacho;
using Nacho.Records;

namespace AchEdit.Windows
{
    /// <summary>
    /// Interaction logic for RecordEditorWindow.xaml
    /// </summary>
    public partial class RecordEditorWindow : Window
    {
        private RecordEditorViewModel ViewModel => (RecordEditorViewModel) DataContext;

        private IDialogsProvider Dialogs { get; }

        private WindowInteropHelper InteropHelper { get; }

        private bool IsReadOnly { get; }

        public RecordEditorWindow(AchRecord record, bool isReadOnly = false)
        {
            InitializeComponent();

            Dialogs = DialogsProvider.Instance;
            ViewModel.Record = record;
            IsReadOnly = isReadOnly;
            InteropHelper = new WindowInteropHelper(this);
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            this.RemoveMaxMinButtons();
        }

        protected void HandleClose(object sender, RoutedEventArgs args)
        {
            Close();
        }

        protected void HandleLoaded(object sender, RoutedEventArgs args) =>
            ViewModel.EditorPage = SelectEditorPage(ViewModel.Record, IsReadOnly);

        protected void HandleSave(object sender, RoutedEventArgs args)
        {
            switch (ViewModel.EditorPage)
            {
                //--- File Records --------------------------------------------
                case FileHeaderEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as FileHeaderRecord);
                    break;
                case FileControlEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as FileControlRecord);
                    break;
                //--- Batch Records -------------------------------------------
                case BatchHeaderEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as BatchHeaderRecord);
                    break;
                case BatchControlEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as BatchControlRecord);
                    break;
                //--- Detail Records ------------------------------------------
                case DetailArcEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as DetailRecordArc);
                    break;
                case DetailBocEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as DetailRecordBoc);
                    break;
                case DetailCcdEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as DetailRecordCcd);
                    break;
                case DetailCieEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as DetailRecordCie);
                    break;
                case DetailCtxEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as DetailRecordCtx);
                    break;
                case DetailDneEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as DetailRecordDne);
                    break;
                case DetailEnrEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as DetailRecordEnr);
                    break;
                case DetailMteEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as DetailRecordMte);
                    break;
                case DetailPopEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as DetailRecordPop);
                    break;
                case DetailPosEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as DetailRecordPos);
                    break;
                case DetailPpdEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as DetailRecordPpd);
                    break;
                case DetailRckEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as DetailRecordRck);
                    break;
                case DetailShrEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as DetailRecordShr);
                    break;
                case DetailTelEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as DetailRecordTel);
                    break;
                case DetailTrcEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as DetailRecordTrc);
                    break;
                case DetailTrxEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as DetailRecordTrx);
                    break;
                case DetailWebEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as DetailRecordWeb);
                    break;
                case DetailXckEditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as DetailRecordXck);
                    break;
                //--- Addenda Records -----------------------------------------
                case AddendaType2EditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as AddendaRecordType2);
                    break;
                case AddendaType5EditorPage page:
                    page.ViewModel.ApplyTo(ViewModel.Record as AddendaRecordType5);
                    break;
            }

            Close();
        }

        private Page SelectEditorPage(AchRecord record, bool isReadOnly) => record switch
        {
            //--- File Records --------------------------------------------
            FileHeaderRecord rec  => new FileHeaderEditorPage(rec, isReadOnly),
            FileControlRecord rec => new FileControlEditorPage(rec, isReadOnly),
            //--- Batch Records -------------------------------------------
            BatchHeaderRecord rec  => new BatchHeaderEditorPage(rec, isReadOnly),
            BatchControlRecord rec => new BatchControlEditorPage(rec, isReadOnly),
            //--- Detail Records ------------------------------------------
            DetailRecordArc rec => new DetailArcEditorPage(rec, isReadOnly),
            DetailRecordBoc rec => new DetailBocEditorPage(rec, isReadOnly),
            DetailRecordCcd rec => new DetailCcdEditorPage(rec, isReadOnly),
            DetailRecordCie rec => new DetailCieEditorPage(rec, isReadOnly),
            DetailRecordCtx rec => new DetailCtxEditorPage(rec, isReadOnly),
            DetailRecordDne rec => new DetailDneEditorPage(rec, isReadOnly),
            DetailRecordEnr rec => new DetailEnrEditorPage(rec, isReadOnly),
            DetailRecordMte rec => new DetailMteEditorPage(rec, isReadOnly),
            DetailRecordPop rec => new DetailPopEditorPage(rec, isReadOnly),
            DetailRecordPos rec => new DetailPosEditorPage(rec, isReadOnly),
            DetailRecordPpd rec => new DetailPpdEditorPage(rec, isReadOnly),
            DetailRecordRck rec => new DetailRckEditorPage(rec, isReadOnly),
            DetailRecordShr rec => new DetailShrEditorPage(rec, isReadOnly),
            DetailRecordTel rec => new DetailTelEditorPage(rec, isReadOnly),
            DetailRecordTrc rec => new DetailTrcEditorPage(rec, isReadOnly),
            DetailRecordTrx rec => new DetailTrxEditorPage(rec, isReadOnly),
            DetailRecordWeb rec => new DetailWebEditorPage(rec, isReadOnly),
            DetailRecordXck rec => new DetailXckEditorPage(rec, isReadOnly),
            //--- Addenda Records -----------------------------------------
            AddendaRecordType2 rec => new AddendaType2EditorPage(rec, isReadOnly),
            AddendaRecordType5 rec => new AddendaType5EditorPage(rec, isReadOnly),
            //-------------------------------------------------------------
            _ => new UnsupportedRecordEditorPage(),
        };
    }
}
