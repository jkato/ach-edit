namespace Nacho
{
    public class Ach
    {
        public const int RECORD_LENGTH = 94;

        public static string[] CreditTransactionCodes { get; } =
        {
            "22", "32", "42", "52", 
        };

        public static string[] DebitTransactionCodes { get; } =
        {
            "27", "37", "47", "57",
        };
    }
}
