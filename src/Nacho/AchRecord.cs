using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Nacho.Extensions;
using PropertyChanged;

namespace Nacho
{
    [DebuggerDisplay("{" + nameof(RecordData) + "}")]
    public class AchRecord : INotifyPropertyChanged
    {
        private static Memory<char> GetEmptyRecord()
        {
            var data = new Memory<char>(new char[Ach.RECORD_LENGTH]);
            data.Span.Fill(' ');
            return data;
        }

        public Memory<char> RecordData { get; }

        [DoNotNotify]
        public virtual string TypeName => GetType().Name;

        public virtual bool IsCreditDetail =>
            RecordTypeCode == '6' && Ach.CreditTransactionCodes.Contains(RecordData.GetString(1, 2));

        public virtual bool IsDebitDetail =>
            RecordTypeCode == '6' && Ach.DebitTransactionCodes.Contains(RecordData.GetString(1, 2));

        [AlsoNotifyFor(nameof(RecordData))]
        public char RecordTypeCode
        {
            get => RecordData.GetChar(0);
            set => RecordData.SetChar(0, value);
        }

        public AchRecord()
        {
            RecordData = new Memory<char>(new char[Ach.RECORD_LENGTH]);
        }

        public AchRecord(string record)
            : this(record.AsSpan())
        { }

        public AchRecord(ReadOnlySpan<char> record)
        {
            if (record.Length > Ach.RECORD_LENGTH)
            {
                RecordData = new Memory<char>(record.Slice(0, Ach.RECORD_LENGTH).ToArray());
            }
            else if (record.Length < Ach.RECORD_LENGTH)
            {
                RecordData = GetEmptyRecord();
                record.CopyTo(RecordData.Span);
            }
            else
            {
                RecordData = new Memory<char>(record.ToArray());
            }
        }

        public AchRecord(ReadOnlyMemory<char> record)
        {
            RecordData = new Memory<char>(new char[record.Length]);
            record.CopyTo(RecordData);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName, object before, object after)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public override string ToString()
        {
            return RecordData.Span.ToString();
        }
    }
}
