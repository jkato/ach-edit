using System;
using System.Globalization;

namespace Nacho.Extensions
{
    public static class MemoryExtensions
    {
        public static char GetChar(this Memory<char> ptr, int index)
        {
            return ptr.Span[index];
        }

        public static void SetChar(this Memory<char> ptr, int index, char c)
        {
            ptr.Span[index] = c;
        }

        public static DateTime? GetDateTime(this Memory<char> ptr, int index, string format)
        {
            var str = GetString(ptr, index, format.Length);
            if (DateTime.TryParseExact(str, format, CultureInfo.InvariantCulture, DateTimeStyles.None,
                out var dt))
            {
                return dt;
            }

            return null;
        }

        public static void SetDateTime(this Memory<char> ptr, int index, string format, DateTime? value)
        {
            var str = value == null
                ? new string(' ', format.Length)
                : value.Value.ToString(format);

            SetString(ptr, index, format.Length, str);
        }

        public static int? GetInt32(this Memory<char> ptr, int index, int length)
        {
            if (int.TryParse(GetString(ptr, index, length), out var val))
            {
                return val;
            }

            return null;
        }

        public static void SetInt32(this Memory<char> ptr, int index, int length, int? value)
        {
            SetString(ptr, index, length, (value ?? 0).ToString(), '0');
        }

        public static long? GetInt64(this Memory<char> ptr, int index, int length)
        {
            if (long.TryParse(GetString(ptr, index, length), out var val))
            {
                return val;
            }

            return null;
        }

        public static void SetInt64(this Memory<char> ptr, int index, int length, long? value)
        {
            SetString(ptr, index, length, (value ?? 0).ToString(), '0');
        }

        public static string GetString(this Memory<char> ptr, int index, int length)
        {
            return ptr.Span[index..(index + length)].ToString().Trim();
        }

        public static void SetString(this Memory<char> ptr, int index, int length, string value, char paddingChar = ' ')
        {
            if (value == null)
                value = new string(' ', length);

            if (value.Length < length)
            {
                switch (paddingChar)
                {
                    case '0':
                        value = value.PadLeft(length, paddingChar);
                        break;
                    default:
                        value = value.PadRight(length, ' ');
                        break;
                }
            }

            if (value.Length > length)
                value = value.Substring(0, length);

            value.AsSpan().CopyTo(ptr.Span.Slice(index, length));
        }
    }
}
