using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nacho.Records;

namespace Nacho
{
    public interface IFileSystem
    {
        List<AchRecord> LoadAch(FileInfo file);

        List<AchRecord> LoadAch(string filePath);

        void WriteAch(FileInfo file, IEnumerable<AchRecord> records, LineEnding lineEnding = LineEnding.CrLf);

        void WriteAch(string filePath, IEnumerable<AchRecord> records, LineEnding lineEnding = LineEnding.CrLf);
    }

    public class FileSystem : IFileSystem
    {
        public List<AchRecord> LoadAch(FileInfo file)
        {
            return LoadAch(file.FullName);
        }

        public List<AchRecord> LoadAch(string filePath)
        {
            using var fs = File.Open(filePath, FileMode.Open, FileAccess.Read);
            using var rd = new BinaryReader(fs, Encoding.ASCII);

            if (fs.Length < Ach.RECORD_LENGTH)
                // File is too small, we can't read any records.
                return new List<AchRecord>();

            // Move past what should be the first record so we can inspect
            // the line ending if any exists.
            fs.Seek(Ach.RECORD_LENGTH, SeekOrigin.Begin);
            var lineEndChar = rd.ReadChar();
            var skipChars = GetSkipCharCount(lineEndChar);

            // Move back to the beginning of the file
            fs.Seek(0, SeekOrigin.Begin);

            var segments = new List<Memory<char>>();
            while (rd.PeekChar() >= 0)
            {
                var buffer = new Memory<char>(rd.ReadChars(Ach.RECORD_LENGTH));
                segments.Add(buffer);

                fs.Seek(fs.Position + skipChars, SeekOrigin.Begin);
            }

            var opts = new ParallelOptions
            {
                MaxDegreeOfParallelism = Environment.ProcessorCount,
            };

            // Parse the records from the original set of data that we read in.
            var records = new ConcurrentDictionary<int, AchRecord>();
            Parallel.ForEach(segments, opts, (seg, state, idx) =>
            {
                var record = ParseRecord(seg);
                records.TryAdd((int) idx, record);
            });

            // Sort the records so they're in order in a collection
            var sortedRecords =  records.OrderBy(kv => kv.Key)
                .Select(kv => kv.Value)
                .ToList();

            // Re-parse detail records into their correct record formats based
            // on the ECC for the nearest batch header.
            var ecc = "";
            for (var i = 0; i < sortedRecords.Count; i++)
            {
                var record = sortedRecords[i];
                if (record is BatchHeaderRecord header)
                {
                    ecc = header.EntryClassCode?.ToUpper() ?? "";
                    continue;
                }

                if (record.RecordTypeCode == '6')
                {
                    sortedRecords[i] = ParseDetailRecord(ecc, record);
                }

                if (record.RecordTypeCode == '7')
                {
                    sortedRecords[i] = ParseAddendaRecord(ecc, record);
                }
            }

            return sortedRecords;
        }

        public void WriteAch(FileInfo file, IEnumerable<AchRecord> records, LineEnding lineEnding = LineEnding.CrLf)
        {
            WriteAch(file.FullName, records, lineEnding);
        }

        public void WriteAch(string filePath, IEnumerable<AchRecord> records, LineEnding lineEnding = LineEnding.CrLf)
        {
            using var fs = File.Open(filePath, FileMode.Create, FileAccess.Write);
            using var wr = new BinaryWriter(fs);

            foreach (var rec in records)
            {
                wr.Write(rec.RecordData.Span);
                switch (lineEnding)
                {
                    case LineEnding.CrLf:
                        wr.Write('\r');
                        wr.Write('\n');
                        break;
                    case LineEnding.Lf:
                        wr.Write('\n');
                        break;
                }
            }

            fs.Flush(true);
        }

        private int GetSkipCharCount(char lineEnding) => lineEnding switch
        {
            '\n' => 1,
            '\r' => 2,
            _    => 0
        };

        private AchRecord ParseRecord(Memory<char> record) => record.Span[0] switch
        {
            '1' => new FileHeaderRecord(record),
            '9' => new FileControlRecord(record),
            '5' => new BatchHeaderRecord(record),
            '8' => new BatchControlRecord(record),
            _   => new AchRecord(record),
        };

        private AchRecord ParseDetailRecord(string ecc, AchRecord record) => ecc switch
        {
            "ARC" => new DetailRecordArc(record.RecordData),
            "BOC" => new DetailRecordBoc(record.RecordData),
            "CCD" => new DetailRecordCcd(record.RecordData),
            "CIE" => new DetailRecordCie(record.RecordData),
            "CTX" => new DetailRecordCtx(record.RecordData),
            "DNE" => new DetailRecordDne(record.RecordData),
            "ENR" => new DetailRecordEnr(record.RecordData),
            "MTE" => new DetailRecordMte(record.RecordData),
            "POP" => new DetailRecordPop(record.RecordData),
            "POS" => new DetailRecordPos(record.RecordData),
            "PPD" => new DetailRecordPpd(record.RecordData),
            "RCK" => new DetailRecordRck(record.RecordData),
            "SHR" => new DetailRecordShr(record.RecordData),
            "TEL" => new DetailRecordTel(record.RecordData),
            "TRC" => new DetailRecordTrc(record.RecordData),
            "TRX" => new DetailRecordTrx(record.RecordData),
            "WEB" => new DetailRecordWeb(record.RecordData),
            "XCK" => new DetailRecordXck(record.RecordData),
            _     => record,
        };

        private AchRecord ParseAddendaRecord(string ecc, AchRecord record) => ecc switch
        {
            "CCD" => new AddendaRecordCcd(record.RecordData),
            "CIE" => new AddendaRecordCie(record.RecordData),
            "CTX" => new AddendaRecordCtx(record.RecordData),
            "DNE" => new AddendaRecordDne(record.RecordData),
            "ENR" => new AddendaRecordEnr(record.RecordData),
            "MTE" => new AddendaRecordMte(record.RecordData),
            "POS" => new AddendaRecordPos(record.RecordData),
            "PPD" => new AddendaRecordPpd(record.RecordData),
            "SHR" => new AddendaRecordShr(record.RecordData),
            "TRX" => new AddendaRecordTrx(record.RecordData),
            "WEB" => new AddendaRecordWeb(record.RecordData),
            _     => record,
        };
    }
}
