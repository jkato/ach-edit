using System;
using System.Linq;

namespace Nacho
{
    public enum LineEnding
    {
        /// <summary>
        /// Don't include any line endings between records in the file.
        /// Everything will effectively be placed on a single line.
        /// </summary>
        None = 0,

        /// <summary>
        /// Use LF as the line ending between records.
        /// </summary>
        Lf = 1,

        /// <summary>
        /// Use CRLF as the line ending between records.
        /// </summary>
        CrLf = 2,
    }

    public static class LineEndings
    {
        public static LineEnding[] All { get; }

        static LineEndings()
        {
            All = Enum.GetValues(typeof(LineEnding)).Cast<LineEnding>().ToArray();
        }
    }
}
