using System;

namespace Nacho.Records
{
    public class AddendaRecordCcd : AddendaRecordType5
    {
        public override string TypeName => "CCD Addenda";

        public AddendaRecordCcd(ReadOnlySpan<char> record)
            : base(record)
        { }

        public AddendaRecordCcd(ReadOnlyMemory<char> record) 
            : base(record)
        { }
    }
}
