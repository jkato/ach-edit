using System;

namespace Nacho.Records
{
    public class AddendaRecordCie : AddendaRecordType5
    {
        public override string TypeName => "CIE Addenda";

        public AddendaRecordCie(ReadOnlySpan<char> record)
            : base(record)
        { }

        public AddendaRecordCie(ReadOnlyMemory<char> record)
            : base(record)
        { }
    }
}
