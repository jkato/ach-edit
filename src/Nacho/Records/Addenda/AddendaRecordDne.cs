using System;

namespace Nacho.Records
{
    public class AddendaRecordDne : AddendaRecordType5
    {
        public override string TypeName => "DNE Addenda";

        public AddendaRecordDne(ReadOnlySpan<char> record)
            : base(record)
        { }

        public AddendaRecordDne(ReadOnlyMemory<char> record)
            : base(record)
        { }
    }
}
