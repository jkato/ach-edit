using System;

namespace Nacho.Records
{
    public class AddendaRecordEnr : AddendaRecordType5
    {
        public override string TypeName => "ENR Addenda";

        public AddendaRecordEnr(ReadOnlySpan<char> record)
            : base(record)
        { }

        public AddendaRecordEnr(ReadOnlyMemory<char> record)
            : base(record)
        { }
    }
}
