using System;

namespace Nacho.Records
{
    public class AddendaRecordMte : AddendaRecordType2
    {
        public override string TypeName => "MTE Addenda";

        public AddendaRecordMte(ReadOnlySpan<char> record)
            : base(record)
        { }

        public AddendaRecordMte(ReadOnlyMemory<char> record)
            : base(record)
        { }
    }
}
