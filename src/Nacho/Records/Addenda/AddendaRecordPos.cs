using System;

namespace Nacho.Records
{
    public class AddendaRecordPos : AddendaRecordType2
    {
        public override string TypeName => "POS Addenda";

        public AddendaRecordPos(ReadOnlySpan<char> record)
            : base(record)
        { }

        public AddendaRecordPos(ReadOnlyMemory<char> record)
            : base(record)
        { }
    }
}
