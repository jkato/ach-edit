using System;

namespace Nacho.Records
{
    public class AddendaRecordPpd : AddendaRecordType5
    {
        public override string TypeName => "PPD Addenda";

        public AddendaRecordPpd(ReadOnlySpan<char> record)
            : base(record)
        { }

        public AddendaRecordPpd(ReadOnlyMemory<char> record)
            : base(record)
        { }
    }
}
