using System;

namespace Nacho.Records
{
    public class AddendaRecordShr : AddendaRecordType2
    {
        public override string TypeName => "SHR Addenda";

        public AddendaRecordShr(ReadOnlySpan<char> record)
            : base(record)
        { }

        public AddendaRecordShr(ReadOnlyMemory<char> record)
            : base(record)
        { }
    }
}
