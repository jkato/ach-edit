using System;

namespace Nacho.Records
{
    public class AddendaRecordTrx : AddendaRecordType5
    {
        public override string TypeName => "TRX Addenda";

        public AddendaRecordTrx(ReadOnlySpan<char> record)
            : base(record)
        { }

        public AddendaRecordTrx(ReadOnlyMemory<char> record)
            : base(record)
        { }
    }
}
