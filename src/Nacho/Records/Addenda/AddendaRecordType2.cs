using System;
using Nacho.Extensions;
using PropertyChanged;

namespace Nacho.Records
{
    public class AddendaRecordType2 : AchRecord
    {
        [AlsoNotifyFor(nameof(RecordData))]
        public string AddendaTypeCode
        {
            get => RecordData.GetString(1, 2);
            set => RecordData.SetString(1, 2, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string ReferenceInformation1
        {
            get => RecordData.GetString(3, 7);
            set => RecordData.SetString(3, 7, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string ReferenceInformation2
        {
            get => RecordData.GetString(10, 3);
            set => RecordData.SetString(10, 3, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string TerminalIdentificationCode
        {
            get => RecordData.GetString(13, 6);
            set => RecordData.SetString(13, 6, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string TransactionSerialNumber
        {
            get => RecordData.GetString(19, 6);
            set => RecordData.SetString(19, 6, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string TransactionDate
        {
            get => RecordData.GetString(25, 4);
            set => RecordData.SetString(25, 4, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string AuthCode
        {
            get => RecordData.GetString(29, 6);
            set => RecordData.SetString(29, 6, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string TerminalLocation
        {
            get => RecordData.GetString(35, 27);
            set => RecordData.SetString(35, 27, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string TerminalCity
        {
            get => RecordData.GetString(62, 15);
            set => RecordData.SetString(62, 15, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string TerminalState
        {
            get => RecordData.GetString(77, 2);
            set => RecordData.SetString(77, 2, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string TraceNumber
        {
            get => RecordData.GetString(79, 15);
            set => RecordData.SetString(79, 15, value);
        }

        public AddendaRecordType2(ReadOnlySpan<char> record)
            : base(record)
        { }

        public AddendaRecordType2(ReadOnlyMemory<char> record)
            : base(record)
        { }
    }
}
