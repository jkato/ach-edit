using System;
using System.Collections.Generic;
using System.Text;

namespace Nacho.Records
{
    public class AddendaRecordWeb : AddendaRecordType5
    {
        public override string TypeName => "WEB Addenda";

        public AddendaRecordWeb(ReadOnlySpan<char> record)
            : base(record)
        { }

        public AddendaRecordWeb(ReadOnlyMemory<char> record)
            : base(record)
        { }
    }
}
