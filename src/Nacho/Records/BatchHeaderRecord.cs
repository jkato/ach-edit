using System;
using Nacho.Extensions;
using PropertyChanged;

namespace Nacho.Records
{
    public class BatchHeaderRecord : AchRecord
    {
        public override string TypeName => "Batch Header";

        [AlsoNotifyFor(nameof(RecordData))]
        public string ServiceClassCode
        {
            get => RecordData.GetString(1, 3);
            set => RecordData.SetString(1, 3, value, '0');
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string CompanyName
        {
            get => RecordData.GetString(4, 16);
            set => RecordData.SetString(4, 16, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string CompanyDiscretionaryData
        {
            get => RecordData.GetString(20, 20);
            set => RecordData.SetString(20, 20, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string CompanyIdentification
        {
            get => RecordData.GetString(40, 10);
            set => RecordData.SetString(40, 10, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string EntryClassCode
        {
            get => RecordData.GetString(50, 3);
            set => RecordData.SetString(50, 3, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string CompanyEntryDescription
        {
            get => RecordData.GetString(53, 10);
            set => RecordData.SetString(53, 10, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string CompanyDescriptiveDate
        {
            get => RecordData.GetString(63, 6);
            set => RecordData.SetString(63, 6, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public DateTime? EffectiveEntryDate
        {
            get => RecordData.GetDateTime(69, "yyMMdd");
            set => RecordData.SetDateTime(69, "yyMMdd", value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public int? SettlementDate
        {
            get => RecordData.GetInt32(75, 3);
            set => RecordData.SetInt32(75, 3, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public char OriginatorStatusCode
        {
            get => RecordData.GetChar(78);
            set => RecordData.SetChar(78, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string OriginatingDfiIdentification
        {
            get => RecordData.GetString(79, 8);
            set => RecordData.SetString(79, 8, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public int? BatchNumber
        {
            get => RecordData.GetInt32(87, 7);
            set => RecordData.SetInt32(87, 7, value);
        }

        public BatchHeaderRecord(ReadOnlySpan<char> record)
            : base(record)
        { }

        public BatchHeaderRecord(ReadOnlyMemory<char> record)
            : base(record)
        { }
    }
}
