using System;
using Nacho.Extensions;
using PropertyChanged;

namespace Nacho.Records
{
    public class DetailRecordPos : AchRecord
    {
        public override string TypeName => "POS Detail";

        [AlsoNotifyFor(nameof(RecordData), nameof(IsCreditDetail), nameof(IsDebitDetail))]
        public string TransactionCode
        {
            get => RecordData.GetString(1, 2);
            set => RecordData.SetString(1, 2, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string ReceivingDfiIdentification
        {
            get => RecordData.GetString(3, 8);
            set => RecordData.SetString(3, 8, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public char CheckDigit
        {
            get => RecordData.GetChar(11);
            set => RecordData.SetChar(11, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string ReceivingRoutingNumber
        {
            get => RecordData.GetString(3, 9);
            set => RecordData.SetString(3, 9, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string DfiAccountNumber
        {
            get => RecordData.GetString(12, 17);
            set => RecordData.SetString(12, 17, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public long? Amount
        {
            get => RecordData.GetInt64(29, 10);
            set => RecordData.SetInt64(29, 10, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string IndividualIdentificationNumber
        {
            get => RecordData.GetString(39, 15);
            set => RecordData.SetString(39, 15, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string IndividualName
        {
            get => RecordData.GetString(54, 22);
            set => RecordData.SetString(54, 22, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string CardTransactionTypeCode
        {
            get => RecordData.GetString(76, 2);
            set => RecordData.SetString(76, 2, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public char AddendaRecordIndicator
        {
            get => RecordData.GetChar(78);
            set => RecordData.SetChar(78, value);
        }

        [AlsoNotifyFor(nameof(RecordData))]
        public string TraceNumber
        {
            get => RecordData.GetString(79, 15);
            set => RecordData.SetString(79, 15, value, '0');
        }

        public DetailRecordPos(ReadOnlySpan<char> record)
            : base(record)
        { }

        public DetailRecordPos(ReadOnlyMemory<char> record)
            : base(record)
        { }
    }
}
